<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');


}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'Santa Clara Realty',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'systemUsers';
         $dbpass = '``1Q2w3e4r5t';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'santaclara';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }

         $pdf->Image('logo.jpg',1,3,-900);

          ;

            $pdf->setFont("Arial",'',8);
           $pdf->Cell(201,4,"Borras Repair and Services, J.P. Cabaguio Avenue, Davao City 8000 ",0,1,"C");
           //$pdf->Cell(201,4,"randsonsproperties@gmail.com|(082) 235 1146|Facebook:R and Sons Properties Co.",0,1,"C");
            $pdf->setFont("Arial",'B',14);
                 $pdf->Ln();
             $pdf->Cell(201,6,"UNCLAIMED CASH COMMISSION",0,1,"C");
                 $pdf->setFont("Arial",'B',10);
           $pdf->Cell(201,5, date("F j, Y", strtotime($_GET['date'])),0,1,"C");
           // Line break
           $pdf->Ln(6);

           $pdf->Cell(38,5,"",0,0,"C");
        $pdf->SetFont('Times','B',10);
           $pdf->Cell(115,5,"UNCLAIMED:",0,0,"L");


           $pdf->Cell(38,5,"",0,1,"C");

    $pdf->SetFont('Times','B',10);

         $sql = "SELECT CONCAT(ag.`AgentFname`,' ', ag.`AgentLname`) AS AgentName,  SUM(comm.`amount`) AS totalAmount FROM commissions comm, propertylists pl, properties pop, clients cl, client__properties cp, agents ag, payments pay
WHERE comm.`cp_id`=cp.`cp_id` AND cp.`client_id`=cl.`client_id` AND pay.`id`=comm.`id`AND pay.`paymentMethod`='Cash' AND comm.`isRelease`='0' AND ag.`agent_id`=comm.`agent_id` AND cp.`propertylistid`=pl.`propertylistid`
AND pl.`propId`=pop.`propId`AND  (comm.`created_at` LIKE '%".$_GET['date']."%' ) GROUP BY ag.agent_id
 ";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                  $lessTax = $row["totalAmount"] - ($row["totalAmount"]* 0.05);
                  $total= $total + $lessTax;





                  $n++;

                //


                //
                $pdf->Cell(38,5,"",0,0,"C");
                $pdf->Cell(5,5,$n,0,0,"C");

                $pdf->Cell(90,5,iconv('UTF-8', 'windows-1252', $row["AgentName"]),0,0,"C");

                $pdf->Cell(20,5,number_format(round($lessTax,2),2,'.',','),0,0,"R");


                $pdf->Cell(38,5,"",0,1,"C");



                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(196,5,"NO RECORDS YET!!",0,1,"C");
         }
         $pdf->Cell(38,7,"",0,0,"C");
    $pdf->SetFont('Times','B',10);
         $pdf->Cell(115,7,"________________________________________________________________",0,0,"C");


         $pdf->Cell(38,7,"",0,1,"C");
         $pdf->Cell(38,3,"",0,0,"C");
         $pdf->Cell(65,3,"TOTAL UNCLAIMED CASH COMM:",0,0,"L");
         $pdf->Cell(50,3,number_format(round($total,2),2,'.',','),0,0,"R");


         $pdf->Cell(38,3,"",0,1,"C");
         $pdf->Cell(38,3,"",0,0,"C");
    $pdf->SetFont('Times','B',10);
         $pdf->Cell(115,3,"________________________________________________________________",0,0,"C");


         $pdf->Cell(38,3,"",0,1,"C");
              $pdf->Ln(6);
         $pdf->Cell(38,5,"",0,0,"C");
      $pdf->SetFont('Times','B',10);
          $pdf->SetTextColor(194,8,8);
         $pdf->Cell(115,5,"CLAIMED CASH COM:",0,0,"L");


         $pdf->Cell(38,5,"",0,1,"C");

                  $sql1 = "SELECT CONCAT(ag.`AgentFname`,' ', ag.`AgentLname`) AS AgentName,  SUM(comm.`amount`) AS totalAmount FROM commissions comm, propertylists pl, properties pop, clients cl, client__properties cp, agents ag, payments pay
         WHERE comm.`cp_id`=cp.`cp_id` AND cp.`client_id`=cl.`client_id` AND pay.`id`=comm.`id`AND pay.`paymentMethod`='Cash' AND comm.`isRelease`='1' AND ag.`agent_id`=comm.`agent_id` AND cp.`propertylistid`=pl.`propertylistid`
         AND pl.`propId`=pop.`propId` AND  (comm.`releaseDate` LIKE '%".$_GET['date']."%' )AND  (comm.`created_at` LIKE '%".$_GET['date']."%' ) GROUP BY ag.agent_id
          ";
                     $result1 = mysqli_query($conn, $sql1);
                     $n1=0;

                     $total1=0;

                  if (mysqli_num_rows($result1) > 0) {
                     while($row1 = mysqli_fetch_assoc($result1)) {
                          $lesstax1 = $row1["totalAmount"] - ($row1["totalAmount"]*0.05);
                           $total1= $total1 +   $lesstax1;





                           $n1++;

                         //


                         //
                         $pdf->Cell(38,5,"",0,0,"C");
                         $pdf->Cell(5,5,$n1,0,0,"C");

                         $pdf->Cell(90,5,iconv('UTF-8', 'windows-1252', $row1["AgentName"]),0,0,"C");

                         $pdf->Cell(20,5,number_format(round($lesstax1,2),2,'.',','),0,0,"R");


                         $pdf->Cell(38,5,"",0,1,"C");



                         }
                         //  $pdf->Cell(5,5,$n,0,0,"C");
                         // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                         // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                         // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                         // }
                         // else
                         // {
                         // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                         // }

                         // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                         // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                         // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                         // $pdf->Cell(26,5,'',0,1,"C");



                  } else {
                      $pdf->Cell(191,5,"NO RECORDS YET!!",0,1,"C");
                  }
                  $pdf->Cell(38,7,"",0,0,"C");
             $pdf->SetFont('Times','B',10);
                  $pdf->Cell(115,7,"________________________________________________________________",0,0,"C");


                  $pdf->Cell(38,7,"",0,1,"C");
                  $pdf->Cell(38,3,"",0,0,"C");
                  $pdf->Cell(57.5,3,"TOTAL CLAIMED CASH COMM:",0,0,"R");
                  $pdf->Cell(57.5,3,number_format(round($total1,2),2,'.',','),0,0,"R");


                  $pdf->Cell(38,3,"",0,1,"C");
                  $pdf->Cell(38,3,"",0,0,"C");
             $pdf->SetFont('Times','B',10);
                  $pdf->Cell(115,3,"________________________________________________________________",0,0,"C");


                  $pdf->Cell(38,3,"",0,1,"C");
                  $pdf->SetTextColor(0,0,0);
                  $pdf->Ln(8);
              $pdf->Cell(140,5 ,"PREPARED BY: " . strtoupper($_GET['user']),0,1,"L");

                 $pdf->SetFont('Times','',6);
              $pdf->Cell(140,3,"CASHIER/AUTHORIZED REPRESENTATIVE",0,1,"L");
                 $pdf->SetFont('Times','B',10);
                   $pdf->Ln(8);
                   $pdf->Cell(140,5,"CHECKED BY: ____________________ ",0,1,"L");
                   $pdf->Ln(8);

              $pdf->Cell(140,5,"APPROVED BY: ____________________ ",0,1,"L");
         mysqli_close($conn);

$pdf->Output();
?>
