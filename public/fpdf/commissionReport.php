<?php
include_once("../fpdf/cellfit.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(2);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',4);
    // Page number
    $this->Cell(0,3,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,3,'Santa Clara Realty',0,1,'C');
    $this->Cell(0,-3,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('P','LEGAL');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'systemUsers';//systemUsers
         $dbpass = '``1Q2w3e4r5t';//``1Q2w3e4r5t for  live
         $dbname = 'santaclara';//*/S3hd%/~]m~X<Zf
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $pdf->Image('logo.jpg',45,8,-600);
         $pdf->Image('logo.jpg',137,8,-600);
         $pdf->setFont("Arial",'B',12);
         $pdf->Cell(196,2,"",0,1,"C");
         $pdf->Cell(196,3,"Santa Clara Land Ventures",0,1,"C");
          $pdf->setFont("Arial",'',8);
          $pdf->Ln(1);
         $pdf->Cell(196,3,"Borras Repair and Services ",0,1,"C");
         $pdf->Cell(196,3,"J.P. Cabaguio Avenue, Davao City 8000",0,1,"C");
         $pdf->Cell(196,3,"Tel no. (082) 321-0703",0,1,"C");
        $pdf->SetFont('Times','B',8);
        

     
          $pdf->Cell(196,3, "Sales from ".date("F j, Y", strtotime($_GET['from'])) . " to " .  date("F j, Y", strtotime($_GET['to'])),0,1,"C");


$pdf->setFont("Arial",'B',10);
        $pdf->Ln(10);
        $pdf->Cell(0,3,'RELEASED COMMISSION REPORT',0,1,'L');

        $pdf->SetFont('Times','B',8);
        $pdf->Cell(33,3.5,"Date Released",1,0,"C");
        $pdf->Cell(39.5,3.5,"Agents Name",1,0,"C");
        $pdf->Cell(39.5,3.5,"Clients Name",1,0,"C");
        $pdf->Cell(13,3.5,"Released",1,0,"C");
        $pdf->Cell(46,3.5,"Property",1,0,"C");
        $pdf->Cell(20,3.5,"Comm",1,1,"C");
        $pdf->SetFont('Times','',9);

         $sql = "SELECT CONCAT(ag.`AgentFname`,' ', ag.`AgentLname`)  AS AgentName, CONCAT(cl.`firstName`, ' ', cl.`lastName`) as Client, comm.comDetails, 
         (comm.`amount`) AS totalAmount, comm.releaseDate, CONCAT(pop.`propertyName`, ' Block: ',pl.`block`, ' Lot: ', pl.`lot`) as propName
          FROM commissions comm, propertylists pl, properties pop, clients cl, client__properties cp, agents ag, payments pay
         WHERE comm.`cp_id`=cp.`cp_id` AND cp.`client_id`=cl.`client_id` AND pay.`id`=comm.`id` AND comm.`isRelease`='1' AND ag.`agent_id`=comm.`agent_id` AND cp.`propertylistid`=pl.`propertylistid`
         AND pl.`propId`=pop.`propId` AND  (comm.`releaseDate` BETWEEN '".$_GET['from']."' AND  '".$_GET['to']."') ORDER BY cl.`lastName`";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $lessTax = $row["totalAmount"] - ($row["totalAmount"]* 0.05);
                $total= $total + $lessTax;
                $n++;
               

              
                $pdf->Cell(33,3,date("F j, Y", strtotime($row["releaseDate"])),0,0,"C");
                $pdf->CellFitScale(39.5,3,$row["AgentName"],0,0,"C");
                $pdf->CellFitScale(39.5,3,$row["Client"],0,0,"C");
                $pdf->Cell(13,3,$row["comDetails"],0,0,"C");
                $pdf->Cell(46,3,$row["propName"],0,0,"C");
                $pdf->Cell(20,3,number_format(($lessTax),2,'.',','),0,1,"C");
          



                }

  $pdf->Ln(6);


         } else {
             $pdf->Cell(196,3,"NO COMMISSIONS YET!!",0,1,"C");
         }
        
          $pdf->SetFont('Times','B',12);
             $pdf->Cell(33,7,"",0,0,"C");
          $pdf->Cell(28.5,7,"",0,0,"C");
          $pdf->Cell(28.5,7," ",0,0,"C");
          $pdf->Cell(33,7,"",0,0,"C");
          $pdf->Cell(36,7,"TOTAL   ",0,0,"C");
         
        //   $pdf->Cell(15,3.5,number_format(round($total1,2),2,'.',','),1,0,"C");
        //   $pdf->Cell(18,3.5,number_format(round($ownerTotal1,2),2,'.',','),1,0,"C");
        //   $pdf->Cell(18,3.5,number_format(round($ownerfixTotal1,2),2,'.',','),1,1,"C");



 

    $pdf->Cell(33,7,number_format(round($total,2),2,'.',','),1,1,"C");
//    $pdf->SetFont('Times','B',10);
//     $pdf->Cell(56.5,3,"",0,1,"C");

//     $pdf->Cell(120,5,"",0,0,"R");
//     $pdf->Cell(30,5,"CASH",0,0,"R");
//     $pdf->Cell(30,5,number_format(round($ownerTotal,2),2,'.',','),0,1,"R");
//     $pdf->Cell(129,5,"",0,0,"R");
//     $pdf->Cell(30,5,"BANK",0,0,"C");
//     $pdf->Cell(30,5,number_format(round($ownerTotal1,2),2,'.',','),0,1,"C");
//     $pdf->Cell(129,5,"",0,0,"R");
//     $pdf->Cell(30,5,"PENALTY",0,0,"C");
//     $pdf->Cell(30,5,number_format(round($total2,2),2,'.',','),0,1,"C");
// $totalna = $ownerTotal + $ownerTotal1 + $total2;

    

    // $pdf->Cell(66,5,"      GROSS INCOME           " . number_format(round($totalna,2),2,'.',','),1,1,"L");




    $pdf->Ln(10);
$pdf->Cell(98,7,"PREPARED BY: " .  strtoupper($_GET['user']) ,0,0,"L");
$pdf->Cell(98,5,"CHECKED BY: ____________________ ",0,1,"L");
   $pdf->SetFont('Times','',6);
$pdf->Cell(98,3,"CASHIER/AUTHORIZED REPRESENTATIVE",0,1,"L");
   $pdf->SetFont('Times','B',10);
     $pdf->Ln(5);
$pdf->Cell(98,7," ",0,0,"L");

$pdf->Cell(98,5,"APPROVED BY: ____________________ ",0,1,"L");
         mysqli_close($conn);

$pdf->Output();
?>
