<?php
include_once("../fpdf/cellfit.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(2);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',4);
    // Page number
    $this->Cell(0,3,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,3,'Santa Clara Realty',0,1,'C');
    $this->Cell(0,-3,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'systemUsers'; //systemUsers
         $dbpass = '``1Q2w3e4r5t';//``1Q2w3e4r5t for testing live
         $dbname = 'santaclara';//*/S3hd%/~]m~X<Zf
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
 

         $pdf->Image('logo.jpg',90,2,-600);
         $pdf->Image('logo.jpg',178,2,-600);
         $pdf->setFont("Arial",'B',12);
         $pdf->Cell(280,2,"",0,1,"C");
         $pdf->Cell(280,3,"Santa Clara Land Ventures",0,1,"C");
          $pdf->setFont("Arial",'',8);
          $pdf->Ln(1);
         $pdf->Cell(280,3,"Borras Repair and Services ",0,1,"C");
         $pdf->Cell(280,3,"J.P. Cabaguio Avenue, Davao City 8000",0,1,"C");
         $pdf->Cell(280,3,"Tel no. (082) 321-0703",0,1,"C");
        $pdf->SetFont('Times','B',8);

        $pdf->Cell(0,3,'CASH REPORT',0,1,'L');
        $pdf->Cell(0,3,date("F j, Y"),0,1,'L');
          $pdf->SetFont('Times','B',6);
        $pdf->Cell(15,3.5,"OR#",1,0,"C");
        $pdf->Cell(20.5,3.5,"Date",1,0,"C");
        $pdf->Cell(30.5,3.5,"Buyers Name",1,0,"C");
        $pdf->Cell(12,3.5,"Installment",1,0,"C");
        $pdf->Cell(15,3.5,"TCP",1,0,"C");
        $pdf->Cell(12,3.5,"SIZE",1,0,"C");
        $pdf->Cell(12,3.5,"TERM",1,0,"C");
        $pdf->Cell(12,3.5,"BLOCK",1,0,"C");
        $pdf->Cell(12,3.5,"LOT",1,0,"C");
        $pdf->Cell(15,3.5,"MONTHLY",1,0,"C");

        $pdf->Cell(15,3.5,"Sales",1,0,"C");
        $pdf->Cell(15,3.5,"10%",1,0,"C");
        $pdf->Cell(15,3.5,"TAX",1,0,"C");
        $pdf->Cell(20,3.5,"100%",1,0,"C");
        $pdf->Cell(59,3.5,"Description",1,1,"C");
        $pdf->SetFont('Times','',8);

         $sql = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`, pay.`payCounter`,cp.`cp_id`,cp.`monthlyAmortization`, (SELECT COUNT(cmid) FROM commissions WHERE id = pay.`id`) as counters, cp.`PlanTerms`,pay.`payCounter`, prop.`propertyName`, plist.`block`, plist.`lot`,plist.`contractPrice`,
         plist.`areasize`, pay.`payment`, pay.`otherpayment`, pay.`paymentDesc`, (cp.`directCommission` + cp.`unitManagerCommission` + cp.`managerCommission` + cp.`grant1`+ cp.`grant2`) AS kinse
         FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl, commissions com WHERE pay.`cp_id`=cp.`cp_id`  AND cp.`propertylistid`=plist.`propertylistid`
AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id`  AND cp.`isActive`='1' AND pay.`paymentMethod`='Cash' AND pay.`isActive`='1' AND (pay.`created_at` LIKE '%".$_GET['date']."%' ) GROUP BY pay.`id`;
";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';

            $kinsePer =0;
            $kinseTotal =0;
            $tax =0;
            $taxTotal =0;
            $owner =0;
            $ownerTotal = 0;
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {



                $total= $total + $row["payment"];
                if($row["counters"]>0){
                  $kinse = $row["kinse"];
                  $tax = $kinse * 0.05;
                  $kinsePer =$kinse - $tax;
                  $taxTotal = $taxTotal + $tax;
                  $kinseTotal = $kinseTotal + $kinsePer;
                  $owner = $row["payment"] - $kinse;
                  $ownerTotal = $ownerTotal + $owner;
                }
                else{
                  $kinse =0;
                  $kinsePer =0;
                    $tax =0;
                    $owner = $row["payment"];
                    $taxTotal = $taxTotal + 0;
                    $kinseTotal = $kinseTotal + 0;
                    $ownerTotal = $ownerTotal + $owner;
                }


                $n++;
                $pdf->Cell(15,3,$row["or_num"],0,0,"C");
                $pdf->Cell(20.5,3,date("F j, Y"),0,0,"C");

                $pdf->CellFitScale(30.5,3,iconv('UTF-8', 'windows-1252', $row["rname"]),0,0,"C");
                if($row["payCounter"]=='1')
                {
                  $pdf->Cell(12,3,$row["payCounter"] . "st",0,0,"C");
                }
                else if($row["payCounter"]=='2')
                {
                  $pdf->Cell(12,3,$row["payCounter"] ."nd",0,0,"C");
                }
                else if($row["payCounter"]=='3')
                {
                  $pdf->Cell(12,3,$row["payCounter"] ."rd",0,0,"C");
                }
                else{
                    $pdf->Cell(12,3,$row["payCounter"] ."th",0,0,"C");
                }

                $pdf->Cell(15,3,number_format(round($row["contractPrice"],2),2,'.',','),0,0,"C");
                $pdf->Cell(12,3,$row["areasize"],0,0,"C");
                $pdf->Cell(12,3,$row["PlanTerms"],0,0,"C");
                $pdf->Cell(12,3,$row["block"],0,0,"C");
                $pdf->Cell(12,3,$row["lot"],0,0,"C");
                $pdf->Cell(15,3,number_format(round($row["monthlyAmortization"],2),2,'.',','),0,0,"C");

                $pdf->Cell(15,3,number_format(round($row["payment"],2),2,'.',','),0,0,"C");
                $pdf->Cell(15,3,number_format(round($kinsePer,2),2,'.',','),0,0,"C");
                $pdf->Cell(15,3,number_format(round($tax,2),2,'.',','),0,0,"C");
                $pdf->Cell(20,3,number_format(round($owner,2),2,'.',','),0,0,"C");
                $pdf->Cell(59.5,3,$row["paymentDesc"],0,1,"C");




                }

  $pdf->Ln(2);


         } else {
             $pdf->Cell(280,3,"NO PAYMENTS YET!!",0,1,"C");
         }
$pdf->Ln(4);
         $pdf->Cell(15,3.5,"",0,0,"C");
         $pdf->Cell(20.5,3.5,"",0,0,"C");
         $pdf->Cell(30.5,3.5," ",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(15,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->SetFont('Times','B',9);
         $pdf->Cell(15,3.5,"TOTAL",0,0,"C");

         $pdf->Cell(15,3.5,number_format(round($total,2),2,'.',','),1,0,"C");
         $pdf->Cell(15,3.5,number_format(round($kinseTotal,2),2,'.',','),1,0,"C");
         $pdf->Cell(15,3.5,number_format(round($taxTotal,2),2,'.',','),1,0,"C");
         $pdf->Cell(20,3.5,number_format(round($ownerTotal,2),2,'.',','),1,0,"C");
         $pdf->Cell(59,3.5,"",0,1,"C");
  $pdf->SetFont('Times','',10);
$pdf->Ln(10);
         $pdf->SetFont('Times','B',8);
         $pdf->Cell(0,3,'PENALTY:',0,1,'L');
         $pdf->Ln(2);
 $pdf->Cell(5,3.5,"#",1,0,"C");

      $pdf->Cell(20.5,3.5,"OR #",1,0,"C");
     $pdf->Cell(36.5,3.5,"Client",1,0,"C");
      $pdf->Cell(25.5,3.5,"Penalty",1,0,"C");
        $pdf->Cell(12,3.5,"#",1,0,"C");
     $pdf->Cell(36.5,3.5,"Property",1,0,"C");

     $pdf->Cell(56.5,3.5,"Description",1,1,"C");
     $pdf->SetFont('Times','',8);

          $sql3 = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`,  prop.`propertyName`, plist.`block`, plist.`lot`, pay.`penalty`, pay.`payCounter`, pay.`otherpayment`,
           pay.`PenaltyDescription` FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
 AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND pay.`paymentMethod`='Cash' AND pay.`penalty`> 0 AND (pay.`created_at` LIKE '%".$_GET['date']."%');
 ";
             $result1 = mysqli_query($conn, $sql3);
             $n1=0;
             $bank1=0;
             $cash1=0;
             $total1=0;
             $repeat1='';
          if (mysqli_num_rows($result1) > 0) {
             while($row1 = mysqli_fetch_assoc($result1)) {



                 $total1= $total1 + $row1["penalty"];



                 $n1++;
                 $pdf->Cell(5,3.5,$n1,0,0,"C");

                 $pdf->Cell(20.5,3.5,$row1["or_num"],0,0,"C");

                  $pdf->Cell(36.5,3.5,iconv('UTF-8', 'windows-1252', $row1["rname"]),0,0,"C");

                 $pdf->Cell(25.5,3.5,number_format(round($row1["penalty"],2),2,'.',','),0,0,"C");
                 if($row1["payCounter"]=='1')
                 {
                   $pdf->Cell(12,3.5,$row1["payCounter"] . "st",0,0,"C");
                 }
                 else if($row1["payCounter"]=='2')
                 {
                   $pdf->Cell(12,3.5,$row1["payCounter"] ."nd",0,0,"C");
                 }
                 else if($row1["payCounter"]=='3')
                 {
                   $pdf->Cell(12,3.5,$row1["payCounter"] ."rd",0,0,"C");
                 }
                 else{
                     $pdf->Cell(12,3.5,$row1["payCounter"] ."th",0,0,"C");
                 }
                 $pdf->Cell(36.5,3.5,$row1["propertyName"] . " blk ". $row1["block"] . " lot ". $row1["lot"] ,0,0,"C");

                 $pdf->Cell(56.5,3.5,$row1["PenaltyDescription"],0,1,"C");




                 }

   $pdf->Ln(3);


          } else {
              $pdf->Cell(180.5,3,"NO PENALTIES YET!!",0,1,"C");
          }
            $pdf->Ln(2);
           $pdf->SetFont('Times','B',8);
         $pdf->Cell(5,3,"",0,0,"C");
    $pdf->Cell(39,3,"",0,0,"C");

    $pdf->Cell(43.5,3,"TOTAL           " . number_format(round($total1,2),2,'.',','),1,0,"C");
   $pdf->SetFont('Times','B',10);
    $pdf->Cell(56.5,3,"",0,1,"C");

    $pdf->Cell(220,5,"",0,0,"R");
    $pdf->Cell(30,5,"SALES",0,0,"C");
    $pdf->Cell(30,5,number_format(round($total,2),2,'.',','),0,1,"C");
    $pdf->Cell(220,1,"",0,0,"R");
    $pdf->Cell(30,5,"PENALTY",0,0,"C");
    $pdf->Cell(30,5,number_format(round($total1,2),2,'.',','),0,1,"C");
$totalna = $total - $total1 - $kinseTotal;
    $pdf->Cell(220,5,"",0,0,"R");
    $pdf->Cell(30,5,"10% Commission",0,0,"C");
    $pdf->Cell(30,5,number_format(round($kinseTotal,2),2,'.',','),0,1,"C");
    $pdf->Cell(214,5,"",0,0,"R");

    $pdf->Cell(66,5,"      GROSS INCOME           " . number_format(round($totalna,2),2,'.',','),1,1,"L");




    $pdf->Ln(5);
$pdf->Cell(140,7,"PREPARED BY: " . strtoupper($_GET['user']),0,0,"L");
$pdf->Cell(140,5,"CHECKED BY: ____________________ ",0,1,"L");
   $pdf->SetFont('Times','',6);
$pdf->Cell(140,3,"CASHIER/AUTHORIZED REPRESENTATIVE",0,1,"L");
   $pdf->SetFont('Times','B',10);
     $pdf->Ln(5);
$pdf->Cell(140,7," ",0,0,"L");

$pdf->Cell(140,5,"APPROVED BY: ____________________ ",0,1,"L");
         mysqli_close($conn);

$pdf->Output();
?>
