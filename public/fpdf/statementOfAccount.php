<?php
include_once("../fpdf/cellfit.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');


}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'Santa Clara Land Ventures',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->Image('logo.jpg',145,6.5,-400);
$pdf->Image('logo.jpg',13,6.5,-400);
$pdf->Ln(5);
$pdf->setFont("Arial",'B',16);
$pdf->Cell(190,5,"STA. CLARA LAND VENTURES",0,1,"C");
$pdf->setFont("Arial",'',12);

$pdf->Cell(190,5,"Borras Repair and Services",0,1,"C");
$pdf->Cell(190,5,"J.P. Cabaguio Avenue, Davao City 8000 ",0,1,"C");
$pdf->Cell(190,5,"Tel no. (082) 321-0703",0,1,"C");
   // $this->setFont("Arial","I",10);
  // $this->Cell(260,5,$_GET["cid"],0,1,"C");
  // Line break
  $pdf->Ln(6);
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'systemUsers'; //systemUsers
         $dbpass = '``1Q2w3e4r5t';//``1Q2w3e4r5t' for testing live
         $dbname = 'santaclara';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $sql1 = "SELECT CONCAT(ag.`AgentFname`, ' ',ag.`AgentLname`) as agent, CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS fName, cl.`client2` , prop.`propertyName`,cp.`PlanTerms`, cp.`isFullyPaid`, pl.`block`, pl.`lot`, cp.`monthlyAmortization`,prop.`address`, cp.`dueDate`, cp.`created_at`,cp.`dateApplied`, pl.`contractPrice`
FROM  agents AS ag, client__properties AS cp, propertylists AS pl, properties AS prop, clients AS cl
WHERE cp.`propertylistid`=pl.`propertylistid` AND pl.`propId`=prop.`propId` AND ag.`agent_id` = cp.`DC` AND cp.`client_id`=cl.`client_id`  AND cp.`cp_id`= '".$_GET['cid']."' ";
            $resultz = mysqli_query($conn, $sql1);
         if ($resultz->num_rows == 1) {

$rowz = $resultz->fetch_assoc();
$today =($rowz["dateApplied"]);
 $month = new DateTime ($rowz["dateApplied"]);
 $month2 = new DateTime (date("Y-m-d"));
 $vieww = $month->diff($month2);
 $full = $rowz["isFullyPaid"];
 $view = (($vieww->format('%y') * 12) + $vieww->format('%m'));
$price = $rowz["contractPrice"];
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"CLIENTS NAME: ",0,0,"L");
$pdf->SetFont('Times','',10);
if($rowz["client2"])
{
  $pdf->CellFitScale(100,5,iconv('UTF-8', 'windows-1252', $rowz["fName"] . " and " . $rowz["client2"]),0,1,"L");
}

else
{
  $pdf->CellFitScale(100,5,iconv('UTF-8', 'windows-1252', $rowz["fName"]),0,1,"L");
}

$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ADDRESS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["propertyName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"BLOCK NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["block"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"LOT NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["lot"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DATE APPLIED.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,date("F  j, Y",strtotime($rowz["created_at"])),0,1,"L");

$pdf->SetFont('Times','B',10);

$pdf->Cell(50,5,"CONTRACT PRICE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,number_format(round($rowz["contractPrice"],2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"PLAN TERMS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["PlanTerms"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"MONTHY AMORTIZATION: ",0,0,"L");
$pdf->SetFont('Arial','',10);

$pdf->Cell(100,5,number_format(round($rowz["monthlyAmortization"],2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DUE DATE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(70,5,$rowz["dueDate"],0,1,"L");
$pdf->SetFont('Times','B',10);

        }
        $pdf->SetFont('Times','B',10);
$pdf->Cell(5,5,"#",1,0,"C");
$pdf->Cell(21,5,"OR #",1,0,"C");
$pdf->Cell(40,5,"For the Month of",1,0,"C");
$pdf->Cell(40,5,"Date of Payment",1,0,"C");
$pdf->Cell(20,5,"Cheque",1,0,"C");
$pdf->Cell(20,5,"Cash",1,0,"C");
$pdf->Cell(20,5,"Bank",1,0,"C");
$pdf->Cell(20,5,"Balance",1,1,"C");
    $pdf->SetFont('Times','',10);

         $sql = "SELECT pay.`or_num`, pay.`paymentName`,pay.`created_at`, pay.`payment`, pay.`paymentMethod`, pay.`otherpayment`
FROM payments pay, client__properties cp, propertylists plist, properties prop WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
AND plist.`propId`=prop.`propId` AND pay.`isActive`='1' AND cp.`cp_id`= '".$_GET['cid']."'
                    ORDER BY pay.`created_at` ";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                 $repeat = strtotime("+1 Month",strtotime($today));
                 if (($row["paymentName"]=="Reservation") || ($row["paymentName"]=="Monthly Payment"))
                  {
                  $total= $total + $row["payment"];
                 }




                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                //


                //
                $pdf->Cell(21,5,$row["or_num"],0,0,"C");
                $pdf->Cell(40,5,date("F  Y",strtotime($today)),0,0,"C");
                $pdf->Cell(40,5,date("F j, Y",strtotime($row["created_at"])),0,0,"C");
                
                if ($row["paymentMethod"]=="Cheque") {
                  $pdf->Cell(20,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
              }
              else
              {
                    $pdf->Cell(20,5,"0.00",0,0,"C");
              }
               
                if ($row["paymentMethod"]=="Cash") {
                    $pdf->Cell(20,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
                }
                else
                {
                      $pdf->Cell(20,5,"0.00",0,0,"C");
                }
                if ($row["paymentMethod"]=="Bank") {
                    $pdf->Cell(20,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
                }
                else
                {
                      $pdf->Cell(20,5,"0.00",0,0,"C");
                }


                $pdf->Cell(20,5,"",0,1,"C");


                  $today = date('Y-m-d',$repeat);
                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(186,5,"NO PAYMENTS YET!!",0,1,"C");
         }
           $pdf->Ln(4);
           $pdf->SetFont('Times','B',10);
         $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(45,5,"",0,0,"C");
    $pdf->Cell(40,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(21,5,number_format(round(($price-$total),2),2,'.',','),1,1,"C");
    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(85,5,"",0,0,"C");
    $pdf->Cell(60,5,"TOTAL PAID: ".number_format(round($total,2),2,'.',',')." "  ,1,0,"C");
    $pdf->Cell(30,5,"" ,0,1,"C");
    $delay = $view-$n;
  if($delay>0 && $full=="0")
    {
      $pdf->Cell(105,5,"Delayed Payments: ". ($delay) . " months",0,0,"L");
    }
    else
    {
      $pdf->Cell(105,5,"",0,0,"L");
    }
    $pdf->Cell(60,5,"",0,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");

    

         mysqli_close($conn);
         $pdf->Ln(10);
         $pdf->Cell(98,7,"PREPARED BY: " .  strtoupper($_GET['user']) ,0,0,"L");
         $pdf->Cell(98,5,"CHECKED BY: ____________________ ",0,1,"L");
            $pdf->SetFont('Times','',6);
         $pdf->Cell(98,3,"CASHIER/AUTHORIZED REPRESENTATIVE",0,1,"L");
            $pdf->SetFont('Times','B',10);
              $pdf->Ln(5);
         $pdf->Cell(98,7," ",0,0,"L");
         
         $pdf->Cell(98,5,"APPROVED BY: ____________________ ",0,1,"L");
$pdf->Output();
?>
