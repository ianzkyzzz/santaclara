<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(2);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',4);
    // Page number
    $this->Cell(0,3,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,3,'R and Sons Properties Co.',0,1,'C');
    $this->Cell(0,-3,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','LEGAL');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'dbuser';//dbuser
         $dbpass = '*/S3hd%/~]m~X<Zf';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'randsons';//*/S3hd%/~]m~X<Zf
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $pdf->Ln(4);

         $pdf->Image('logo.jpg',10,3,-800);
         $pdf->setFont("Times",'B',18);

         $pdf->Cell(196,5,strtoupper($_GET['propertyName']),0,1,"C");

          $pdf->setFont("Arial",'B',12);
          $pdf->Cell(196,5,strtoupper($_GET['partnerName']),0,1,"C");
            $pdf->setFont("Arial",'B',8);
          $pdf->Cell(196,5,date("F j, Y", strtotime($_GET['from'])) . " - " .  date("F j, Y", strtotime($_GET['To'])),0,1,"C");


$pdf->setFont("Arial",'B',10);
        $pdf->Ln(4);
        $pdf->Cell(0,3,'CASH REPORT',0,1,'L');

        $pdf->SetFont('Times','B',8);
        $pdf->Cell(15,3.5,"OR#",1,0,"C");
        $pdf->Cell(20.5,3.5,"Date",1,0,"C");
        $pdf->Cell(28.5,3.5,"Buyers Name",1,0,"C");
        $pdf->Cell(14,3.5,"Installment",1,0,"C");
        $pdf->Cell(15,3.5,"TCP",1,0,"C");
        $pdf->Cell(12,3.5,"SIZE",1,0,"C");
        $pdf->Cell(12,3.5,"TERM",1,0,"C");
        $pdf->Cell(12,3.5,"BLOCK",1,0,"C");
        $pdf->Cell(12,3.5,"LOT",1,0,"C");
        $pdf->Cell(15,3.5,"Sales",1,0,"C");
        $pdf->Cell(18,3.5,$_GET['percentage']."% Share",1,0,"C");
        $pdf->Cell(18,3.5,"100%",1,1,"C");
        $pdf->SetFont('Times','',9);

         $sql = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`, pay.`payCounter`,cp.`cp_id`,cp.`monthlyAmortization`, (SELECT COUNT(cmid) FROM commissions WHERE id = pay.`id`) as counters, cp.`PlanTerms`,pay.`payCounter`, prop.`propertyName`, plist.`block`, plist.`lot`,plist.`contractPrice`,
         plist.`areasize`, pay.`payment`, pay.`otherpayment`,pay.`created_at`, pay.`paymentDesc`, (cp.`directCommission` + cp.`unitManagerCommission` + cp.`managerCommission` + cp.`grant1`+ cp.`grant2` + cp.`grant3` + cp.`grant4` + cp.`grant5`) AS kinse
         FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl, commissions com WHERE pay.`cp_id`=cp.`cp_id`  AND cp.`propertylistid`=plist.`propertylistid`
AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND pay.`paymentMethod`='Cash' AND prop.`propId` = '".$_GET['propId']."' AND pay.`isActive`='1' AND (pay.`created_at` BETWEEN  '".$_GET['from']."' AND '".$_GET['To']."') GROUP BY pay.`id`
";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';

            $kinsePer =0;
            $kinseTotal =0;
            $tax =0;
            $taxTotal =0;
            $owner =0;
            $ownerTotal = 0;
            $ownerfixTotal = 0;
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {



                $total= $total + $row["payment"];
                if($row["counters"]>0){
                  $kinse = $row["kinse"];
                  $tax = $kinse * 0.05;
                  $kinsePer =$kinse - $tax;
                  $taxTotal = $taxTotal + $tax;
                  $kinseTotal = $kinseTotal + $kinsePer;
                  $owner = ($row["payment"] - $kinse)*($_GET['percentage']/100);
                  $ownerfix = $row["payment"] - $kinse;
                  $ownerTotal = $ownerTotal + $owner;
                  $ownerfixTotal = $ownerfixTotal + $ownerfix;

                }
                else{
                  $kinse =0;
                  $kinsePer =0;
                    $tax =0;
                    $owner = $row["payment"] * ($_GET['percentage']/100);
                    $ownerfix = $row["payment"];
                    $taxTotal = $taxTotal + 0;
                    $kinseTotal = $kinseTotal + 0;
                    $ownerTotal = $ownerTotal + $owner;
                    $ownerfixTotal = $ownerfixTotal + $ownerfix;
                }


                $n++;
                $pdf->Cell(15,3,$row["or_num"],0,0,"C");
                $pdf->Cell(20.5,3, date("F j, Y", strtotime($row['created_at'])),0,0,"C");

                $pdf->Cell(28.5,3,iconv('UTF-8', 'windows-1252', $row["rname"]),0,0,"C");
                if($row["payCounter"]=='1')
                {
                  $pdf->Cell(14,3,$row["payCounter"] . "st",0,0,"C");
                }
                else if($row["payCounter"]=='2')
                {
                  $pdf->Cell(14,3,$row["payCounter"] ."nd",0,0,"C");
                }
                else if($row["payCounter"]=='3')
                {
                  $pdf->Cell(14,3,$row["payCounter"] ."rd",0,0,"C");
                }
                else{
                    $pdf->Cell(14,3,$row["payCounter"] ."th",0,0,"C");
                }

                $pdf->Cell(15,3,number_format(round($row["contractPrice"],2),2,'.',','),0,0,"C");
                $pdf->Cell(12,3,$row["areasize"],0,0,"C");
                $pdf->Cell(12,3,$row["PlanTerms"],0,0,"C");
                $pdf->Cell(12,3,$row["block"],0,0,"C");
                $pdf->Cell(12,3,$row["lot"],0,0,"C");
                $pdf->Cell(15,3,number_format(round($row["payment"],2),2,'.',','),0,0,"C");
                $pdf->Cell(18,3,number_format(round($owner),2,'.',','),0,0,"C");
                $pdf->Cell(18.5,3,$ownerfix,0,1,"C");




                }

  $pdf->Ln(2);


         } else {
             $pdf->Cell(196,3,"NO PAYMENTS YET!!",0,1,"C");
         }
         $pdf->SetFont('Times','B',9);
            $pdf->Cell(15,3.5,"",0,0,"C");
         $pdf->Cell(20.5,3.5,"",0,0,"C");
         $pdf->Cell(30.5,3.5," ",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(15,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(12,3.5,"",0,0,"C");
         $pdf->Cell(15,3.5,number_format(round($total,2),2,'.',','),1,0,"C");
         $pdf->Cell(18,3.5,number_format(round($ownerTotal,2),2,'.',','),1,0,"C");
         $pdf->Cell(18,3.5,number_format(round($ownerfixTotal,2),2,'.',','),1,1,"C");
         $pdf->Cell(0,3,'BANK REPORT',0,1,'L');
           $pdf->SetFont('Times','B',8);
         $pdf->Cell(15,3.5,"OR#",1,0,"C");
         $pdf->Cell(20.5,3.5,"Date",1,0,"C");
         $pdf->Cell(28.5,3.5,"Buyers Name",1,0,"C");
         $pdf->Cell(14,3.5,"Installment",1,0,"C");
         $pdf->Cell(15,3.5,"TCP",1,0,"C");
         $pdf->Cell(12,3.5,"SIZE",1,0,"C");
         $pdf->Cell(12,3.5,"TERM",1,0,"C");
         $pdf->Cell(12,3.5,"BLOCK",1,0,"C");
         $pdf->Cell(12,3.5,"LOT",1,0,"C");
         $pdf->Cell(15,3.5,"Sales",1,0,"C");
         $pdf->Cell(18,3.5, $_GET["percentage"]."% Share",1,0,"C");
         $pdf->Cell(18,3.5,"100%",1,1,"C");
         $pdf->SetFont('Times','',9);

          $sql1 = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`, pay.`payCounter`,cp.`cp_id`,cp.`monthlyAmortization`, (SELECT COUNT(cmid) FROM commissions WHERE id = pay.`id`) as counters, cp.`PlanTerms`,pay.`payCounter`,pay.`bank`, prop.`propertyName`, plist.`block`, plist.`lot`,plist.`contractPrice`,
          plist.`areasize`, pay.`payment`,pay.`created_at`, pay.`otherpayment`, pay.`paymentDesc`, (cp.`directCommission` + cp.`unitManagerCommission` + cp.`managerCommission` + cp.`grant1`+ cp.`grant2` + cp.`grant3` + cp.`grant4` + cp.`grant5`) AS kinse
          FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl, commissions com WHERE pay.`cp_id`=cp.`cp_id`  AND cp.`propertylistid`=plist.`propertylistid`
         AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND pay.`paymentMethod`='Bank' AND prop.`propId` = '".$_GET['propId']."' AND pay.`isActive`='1' AND pay.`isActive`='1' AND (pay.`created_at` BETWEEN  '".$_GET['from']."' AND '".$_GET['To']."') GROUP BY pay.`id`;
         ";
             $result1 = mysqli_query($conn, $sql1);
             $n=0;
             $bank=0;
             $cash=0;
             $total=0;
             $repeat='';

             $kinsePer1 =0;
             $kinseTotal1 =0;
             $tax1 =0;
             $taxTotal1 =0;
             $owner1 =0;
             $ownerTotal1 = 0;
             $total1= 0;
             $ownerfixTotal1 =0;
          if (mysqli_num_rows($result1) > 0) {
             while($row1 = mysqli_fetch_assoc($result1)) {



                 $total1= $total1 + $row1["payment"];
                 if($row1["counters"]>0){

                   $kinse1 =  $row1["kinse"];
                   $tax1 = $kinse1 * 0.05;
                   $kinsePer1 =$kinse1 - $tax1;
                   $taxTotal1 = $taxTotal1 + $tax1;
                   $kinseTotal1 = $kinseTotal1 + $kinsePer1;
                   $owner1 = ($row1["payment"] - $kinse1)*($_GET['percentage']/100);
                   $owner1fix = $row1["payment"] - $kinse1;
                   $ownerTotal1 = $ownerTotal1 + $owner1;
                   $ownerfixTotal1 = $ownerfixTotal1 + $owner1fix;
                 }
                 else{
                   $kinse1 =0;
                   $kinsePer1 =0;
                     $tax1 =0;
                     $owner1 = $row1["payment"]*($_GET['percentage']/100);
                     $owner1fix = $row1["payment"];
                     $taxTotal1 = $taxTotal1 + 0;
                     $kinseTotal1 = $kinseTotal1 + 0;
                     $ownerTotal1 = $ownerTotal1 + $owner1;
                     $ownerfixTotal1 = $ownerfixTotal1 + $owner1fix;
                 }


                 $n++;
                 $pdf->Cell(15,3,$row1["or_num"],0,0,"C");
                 $pdf->Cell(20.5,3,date("F j, Y", strtotime($row1['created_at'])),0,0,"C");

                 $pdf->Cell(28.5,3,iconv('UTF-8', 'windows-1252', $row1["rname"]),0,0,"C");
                 if($row1["payCounter"]=='1')
                 {
                   $pdf->Cell(14,3,$row1["payCounter"] . "st",0,0,"C");
                 }
                 else if($row1["payCounter"]=='2')
                 {
                   $pdf->Cell(14,3,$row1["payCounter"] ."nd",0,0,"C");
                 }
                 else if($row1["payCounter"]=='3')
                 {
                   $pdf->Cell(14,3,$row1["payCounter"] ."rd",0,0,"C");
                 }
                 else{
                     $pdf->Cell(14,3,$row1["payCounter"] ."th",0,0,"C");
                 }

                 $pdf->Cell(15,3,number_format(round($row1["contractPrice"],2),2,'.',','),0,0,"C");
                 $pdf->Cell(12,3,$row1["areasize"],0,0,"C");
                 $pdf->Cell(12,3,$row1["PlanTerms"],0,0,"C");
                 $pdf->Cell(12,3,$row1["block"],0,0,"C");
                 $pdf->Cell(12,3,$row1["lot"],0,0,"C");
                 $pdf->Cell(15,3,number_format(round($row1["payment"],2),2,'.',','),0,0,"C");
                 $pdf->Cell(18,3,number_format(round($owner1,2),2,'.',','),0,0,"C");
                 $pdf->Cell(18.5,3,$owner1fix,0,1,"C");




                 }

         $pdf->Ln(2);


          } else {
              $pdf->Cell(196,3,"NO PAYMENTS YET!!",0,1,"C");
          }
          $pdf->SetFont('Times','B',9);
             $pdf->Cell(15,3.5,"",0,0,"C");
          $pdf->Cell(20.5,3.5,"",0,0,"C");
          $pdf->Cell(30.5,3.5," ",0,0,"C");
          $pdf->Cell(12,3.5,"",0,0,"C");
          $pdf->Cell(15,3.5,"",0,0,"C");
          $pdf->Cell(12,3.5,"",0,0,"C");
          $pdf->Cell(12,3.5,"",0,0,"C");
          $pdf->Cell(12,3.5,"",0,0,"C");
          $pdf->Cell(12,3.5,"",0,0,"C");
          $pdf->Cell(15,3.5,number_format(round($total1,2),2,'.',','),1,0,"C");
          $pdf->Cell(18,3.5,number_format(round($ownerTotal1,2),2,'.',','),1,0,"C");
          $pdf->Cell(18,3.5,number_format(round($ownerfixTotal1,2),2,'.',','),1,1,"C");



  $pdf->SetFont('Times','',10);
$pdf->Ln(10);
         $pdf->SetFont('Times','B',8);
         $pdf->Cell(0,3,'PENALTY:',0,1,'L');
         $pdf->Ln(2);
 $pdf->Cell(5,3.5,"#",1,0,"C");

      $pdf->Cell(20.5,3.5,"OR #",1,0,"C");
     $pdf->Cell(36.5,3.5,"Client",1,0,"C");
      $pdf->Cell(25.5,3.5,"Penalty",1,0,"C");
        $pdf->Cell(12,3.5,"#",1,0,"C");
     $pdf->Cell(36.5,3.5,"Property",1,0,"C");

     $pdf->Cell(56.5,3.5,"Description",1,1,"C");
     $pdf->SetFont('Times','',8);

          $sql3 = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS rname,pay.`or_num`,  prop.`propertyName`, plist.`block`, plist.`lot`, pay.`penalty`, pay.`payCounter`, pay.`otherpayment`,
           pay.`PenaltyDescription` FROM payments pay, client__properties cp, propertylists plist, properties prop, clients cl WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
 AND plist.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id` AND prop.`propId` = '".$_GET['propId']."' AND pay.`paymentMethod`='Cash' AND pay.`penalty`> 0 AND (pay.`created_at` BETWEEN  '%".$_GET['from']."%' AND '%".$_GET['To']."%'  );
 ";
             $result2 = mysqli_query($conn, $sql3);
             $n2=0;
             $bank2=0;
             $cash2=0;
             $total2=0;
             $repeat2='';
          if (mysqli_num_rows($result2) > 0) {
             while($row2 = mysqli_fetch_assoc($result2)) {



                 $total2= $total2 + ($row2["penalty"]*($_GET['percentage']/100));



                 $n2++;
                 $pdf->Cell(5,3.5,$n2,0,0,"C");

                 $pdf->Cell(20.5,3.5,$row2["or_num"],0,0,"C");

                  $pdf->Cell(36.5,3.5,iconv('UTF-8', 'windows-1252', $row2["rname"]),0,0,"C");

                 $pdf->Cell(25.5,3.5,number_format(round(($row2["penalty"]*($_GET['percentage']/100)),2),2,'.',','),0,0,"C");
                 if($row2["payCounter"]=='1')
                 {
                   $pdf->Cell(12,3.5,$row2["payCounter"] . "st",0,0,"C");
                 }
                 else if($row2["payCounter"]=='2')
                 {
                   $pdf->Cell(12,3.5,$row2["payCounter"] ."nd",0,0,"C");
                 }
                 else if($row2["payCounter"]=='3')
                 {
                   $pdf->Cell(12,3.5,$row2["payCounter"] ."rd",0,0,"C");
                 }
                 else{
                     $pdf->Cell(12,3.5,$row2["payCounter"] ."th",0,0,"C");
                 }
                 $pdf->Cell(36.5,3.5,$row2["propertyName"] . " blk ". $row2["block"] . " lot ". $row2["lot"] ,0,0,"C");

                 $pdf->Cell(56.5,3.5,$row2["PenaltyDescription"],0,1,"C");




                 }

   $pdf->Ln(3);


          } else {
              $pdf->Cell(196,3,"NO PENALTIES YET!!",0,1,"C");
          }
            $pdf->Ln(2);
           $pdf->SetFont('Times','B',8);
         $pdf->Cell(5,3,"",0,0,"C");
    $pdf->Cell(39,3,"",0,0,"C");

    $pdf->Cell(43.5,3,"TOTAL           " . number_format(round($total2,2),2,'.',','),1,0,"C");
   $pdf->SetFont('Times','B',10);
    $pdf->Cell(56.5,3,"",0,1,"C");

    $pdf->Cell(120,5,"",0,0,"R");
    $pdf->Cell(30,5,"CASH",0,0,"R");
    $pdf->Cell(30,5,number_format(round($ownerTotal,2),2,'.',','),0,1,"R");
    $pdf->Cell(129,5,"",0,0,"R");
    $pdf->Cell(30,5,"BANK",0,0,"C");
    $pdf->Cell(30,5,number_format(round($ownerTotal1,2),2,'.',','),0,1,"C");
    $pdf->Cell(129,5,"",0,0,"R");
    $pdf->Cell(30,5,"PENALTY",0,0,"C");
    $pdf->Cell(30,5,number_format(round($total2,2),2,'.',','),0,1,"C");
$totalna = $ownerTotal + $ownerTotal1 + $total2;

    $pdf->Cell(125,5,"",0,0,"R");

    $pdf->Cell(66,5,"      GROSS INCOME           " . number_format(round($totalna,2),2,'.',','),1,1,"L");




    $pdf->Ln(10);
$pdf->Cell(98,7,"PREPARED BY: " .  strtoupper($_GET['user']) ,0,0,"L");
$pdf->Cell(98,5,"CHECKED BY: ____________________ ",0,1,"L");
   $pdf->SetFont('Times','',6);
$pdf->Cell(98,3,"CASHIER/AUTHORIZED REPRESENTATIVE",0,1,"L");
   $pdf->SetFont('Times','B',10);
     $pdf->Ln(5);
$pdf->Cell(98,7," ",0,0,"L");

$pdf->Cell(98,5,"APPROVED BY: ____________________ ",0,1,"L");
         mysqli_close($conn);

$pdf->Output();
?>
