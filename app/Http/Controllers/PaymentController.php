<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Commission;
use App\Models\Agent;
use App\Models\Bill;
use App\Models\Client_Property;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
  public function index()
  {
    $id = Auth::id();
    if($id>0)
    {
          return view('payment.payment');
    }
    else
    {

        return view('auth.login')->withErrors(['You Are Not Login!!!']);
}

  }
  public function scheduleSMS()
  {
    $data = DB::select(DB::raw('SELECT cl.firstName,cl.lastName, cp.dueDate, pr.`propertyName`, pl.`block`, pl.`lot`,cl.`mobileNumber` FROM client__properties cp, clients cl, properties pr, propertylists pl WHERE
 (cp.dueDate = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 DAY), "%d")) AND cp.client_id=cl.client_id AND cp.propertylistid=pl.propertylistid AND pl.propId=pr.propId AND cp.`isActive`="1" AND cp.`isFullyPaid`="0"'));




foreach($data as $item)
{
  $apicode="ST-DSRDO875467_BTAET";
  $passwd="8trubl&qfs";
  $number=$item->mobileNumber;
  $message = "Hi " . $item->firstName. " " .$item->lastName. ", This is a friendly reminder, Your property located at" .$item->propertyName ." on block ". $item->block.", lot ".$item->lot." dated every " . $item->dueDate." of the month. Please pay on or before due date. Thank You! Message from Diamanete Land Development Realty Services";
   $this->itexmo($number,$message,$apicode,$passwd);
}

  }
  public function paymenthistory()
  {
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('payments.otherpayment','payments.paymentMethod','payments.or_num','client__properties.totalPaid','client__properties.cp_id','payments.payment','payments.created_at','payments.id','payments.paymentName','payments.paymentMethod','payments.paymentDesc','properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','clients.firstName','clients.lastName')
    ->where('payments.isActive', '=', 1)
    ->get();
     return view('paymenthistory.paymenthistory',['data'=>$data])->with('count',1);

  }
  public function Edit(Request $request)
  {
    $cp_id = $request->input('cpid');
    $payid = $request->input('payid');
    $ornum = $request->input('ornum');
    $payment = $request->input('payment');
    $Method = $request->input('Method');
    $date = $request->input('date');
    $total = $request->input('totalPaid');
    $oldPayment = $request->input('originalpayment');
    $newTotal = ($total - $oldPayment) + $payment;

      $data1=Payment::find($payid);
      $data1->or_num=$ornum;
      $data1->payment=$payment;
      $data1->paymentMethod=$Method;
      $data1->created_at=$date;
      $data1->save();

    $this->updateTotalPaid($cp_id,$newTotal);
    return redirect()->back()->with('message', 'Payment Edited Successfully');
  }
  public function unlist($id,$amount)
  {

      $data1=Payment::find($id);
      $data1->isActive='0';
      $data1->save();
      $this->updatePropertyDelete($id,$amount);
      $this->updateCommissionDelete($id);


    return redirect()->back()->with('message', 'Payment Edited Successfully');
  }

  public function otherindex()
    {
        return view('payment.otherpayment');
    }
public function SaveOtherPayment(Request $request)
{
  $finalBranch =  Auth::user()->branch;
  $id = Auth::id();
$comm =  0;
$cp_id  = $request->input('propList');
  $request->validate([
    'propList' => 'required',
    'Monthly' => 'required',
    'paymentfor' => 'required',
    'details' => 'required',
    'OR' => 'required',

  ]);

 $pay = new Payment();
 $pay->user_id =  $id;
 $pay->branch = $finalBranch;
 $pay->Commission = $comm;
 $pay->or_num = $request->input('OR');
 $pay->cp_id = $request->input('propList');
 $pay->payment = $request->input('Monthly');
 $pay->penalty = $request->input('Penalty');
 $pay->paymentName = $request->input('paymentfor');
 $pay->paymentMethod = 'Cash';
 $pay->paymentDesc = $request->input('details');
 $pay->save();
 return redirect()->back()->with('message', 'Payment Added Successfully');


}
private function storeBilling($id,$cp_id,$ornum)
{
  $type="payment";
  $bill = new Bill();

   $bill->id =  $id;
   $bill->cp_id = $cp_id;
   $bill->type = $type;
   $bill->orcn =$ornum;
$bill->save();
}
public function saveCommission($id,$cpid,$agent_id,$commission,$details,$underpaid)
{

 $comm = new Commission();
 $comm->id =  $id;
 $comm->agent_id = $agent_id;
 $comm->amount = $commission;
 $comm->cp_id =$cpid;
 $comm->comDetails = $details;
 $comm->underpaid = $underpaid;
 $comm->save();
}
public function updatePropertyDelete($id,$amount)
{
  $data = DB::table('payments')
  ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
  ->select('client__properties.counter','client__properties.comRelease','client__properties.totalPaid','client__properties.cp_id')
  ->where('payments.id',$id)
  ->get();
  $counter=($data[0]->counter)-1;
  $comRelease=($data[0]->comRelease)-1;
  $totalPaid =($data[0]->totalPaid)-$amount;
  $data1=Client_Property::find($data[0]->cp_id);
  $data1->counter=$counter;
  $data1->comRelease=$comRelease;
  $data1->totalPaid=$totalPaid;
  $data1->save();
}
public function updateCommissionDelete($id)
{
  $updateDetails = [
      'isDelete' => '1'
  ];
  DB::table('commissions')
      ->where('id', $id)
      ->update($updateDetails);
}
public function updateCommissionBackdate($id,$date)
{
  $updateDetails = [
      'isRelease' => '1', 'releaseDate' => $date
  ];
  DB::table('commissions')
      ->where('id', $id)
      ->update($updateDetails);
}
  public function Save(Request $request)
  {
    $dateNow = date('Y-m-d');
    if(!$request->input('date'))
    {
      $dateRecord = date('Y-m-d');
    }
    else{
      $dateRecord = $request->input('date');
    }
    
    $underpaid = $request->input('underpaid');
    $idForPayment = $request->input('idForPayment');
    $hasUnderpayed = $request->input('hasUnderpayed');
    $underpaidBalance = $request->input('underpaidBalance');
    $payCounter = $request->input('counter');
    $commRell = $request->input('comRelease')-1;
    $commRellCHeck = $request->input('comRelease');
    $amount = $request->input('Paid') + $request->input('Monthly');
    $monthly =$request->input('monthlyAmortization');
    
    $balance = $request->input('Balance')-$request->input('Monthly');
    $x=1;
    for ($i=0; $i <$request->input('multiplier') ; $i++) {
     $id = Auth::id();
    $branch = DB::table('users')
    ->select('branch')
    ->where('id',$id)
    ->get();
 
    $directAgentID = $request->input('directAgent');
    $unitManagerID = $request->input('unitManager');
    $managerID = $request->input('manager');
    $grant1 = $request->input('gcom1');
    $grant2 = $request->input('gcom2');

$finalBranch = ($branch[0]->branch);


$currentRelCom = $request->input('numberRelease') - $commRell;
$comdetails = ($currentRelCom . '/' . $request->input('numberRelease'));


$cp_id  = $request->input('propList');
if($commRellCHeck<0) {
  $dcom =0;
  $umcom = 0;
  $mcom = 0;
  $gcom1 = 0;
  $gcom2 = 0;

}
else
{
  if ($request->input('cusCommission')=="") {
  $dcom = $request->input('dcom');
  $umcom = $request->input('umcom');
  $mcom = $request->input('mcom');
  $gcom1 = $request->input('grant1');
  $gcom2 = $request->input('grant2');
  }


}

if ($balance<=0) {

     $this->nowFullPaid($cp_id);
}

    $request->validate([
      'propList' => 'required',
      'Monthly' => 'required',
      'PaymentMethod' => 'required',
      'details' => 'required',
      'OR' => 'required',

    ]);

   $pay = new Payment();
   $pay->user_id =  $id;
   $pay->branch = $finalBranch;

   $pay->or_num = $request->input('OR');
   $pay->cp_id = $request->input('propList');
   $pay->payment = $request->input('FinalMonthly');
   $pay->penalty = $request->input('Penalty');
   $pay->paymentName = 'Monthly Payment';
   $pay->bank = $request->input('bank');
   $pay->created_at = $dateRecord;
   $pay->PenaltyDescription = $request->input('PenaltyDescription');
   $pay->payCounter = $payCounter;
   $pay->paymentMethod = $request->input('PaymentMethod');
   $pay->paymentDesc = $request->input('details');

   $pay->save();
   $id = $pay->id;
   if($id){

      if($hasUnderpayed==1)
      {
        $this->updatePaymentUnderPaid($idForPayment);
        // $this->updatePaymentUnderPaid2($idForPayment,$monthly);
      }
        $this->updatePropertyPaid($cp_id,$amount,$commRell,$payCounter,$underpaidBalance,$id,$underpaid);
       
     if($commRellCHeck > 0) {
       $this->saveCommission($id,$request->input('propList'),$directAgentID,$dcom, $comdetails,$underpaid);
       if ($unitManagerID) {
          $this->saveCommission($id,$request->input('propList'),$unitManagerID,$umcom, $comdetails,$underpaid);
       }
       if ($managerID) {
          $this->saveCommission($id,$request->input('propList'),$managerID,$mcom, $comdetails,$underpaid);
       }
       if ($grant1) {
          $this->saveCommission($id,$request->input('propList'),$grant1,$gcom1,$comdetails,$underpaid);
       }
       if ($grant2) {
          $this->saveCommission($id,$request->input('propList'),$grant2,$gcom2,$comdetails,$underpaid);
       }
       if($dateNow !=$request->input('date'))
       {
        $this->updateCommissionBackdate($id,$request->input('date'));
       }
   }
  //  $this->sendSMS($id);

   }
   $x++;
   $payCounter++;
   $commRell--;
   $commRellCHeck--;
  
  }

   return redirect()->back()->with('message', 'Payment Successfully');


  }
  public function sendSMS($id)
  {

$data = DB::table('payments')
->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
->join('properties', 'properties.propId', '=', 'propertylists.propId')
->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS Cname")),'clients.mobileNumber','payments.id','propertylists.contractPrice','client__properties.totalPaid','client__properties.dueDate','properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at')
->where('payments.id', '=', $id)
->get();

    $apicode="ST-DSRDO875467_BTAET";
    $passwd="8trubl&qfs";
    $commission = ($data[0]->totalPaid/$data[0]->contractPrice);
    $propertyName 	=$data[0]->propertyName;
    $name 			=$data[0]->Cname;
    $number         =$data[0]->mobileNumber;
    $lotno          =$data[0]->lot;
    $blockno        =$data[0]->block;
    $due            =$data[0]->dueDate;
    $date           =$data[0]->created_at;
    $message = "Hi " . $name . " we have just recieved your payment to your property located at " . $propertyName . " Block:" . $blockno . " lot:" . $lotno  . " Dated: ".date("F  j, Y",strtotime($date)) . ". Thank you and God Bless from Santa Clara Land Ventures";
    if (strlen($number)==11) {
     $this->itexmo($number,$message,$apicode,$passwd);
    }


  }
  public function itexmo($number,$message,$apicode,$passwd){
		$url = 'https://www.itexmo.com/php_api/api.php';
		$itexmo = array('1' => $number, '2' => $message, '3' => $apicode, 'passwd' => $passwd);
		$param = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($itexmo),
			),
		);
		$context  = stream_context_create($param);
		return file_get_contents($url, false, $context);
}
  private function updatePropertyPaid($cp_id,$amount,$commRell,$counter,$underpaidBalance,$id,$underpaid)
  {
    $finalCount = $counter +1;
    $data=Client_Property::find($cp_id);
    $data->totalPaid=$amount;
    $data->counter=$finalCount;
    $data->BalanceMonthly=$underpaidBalance;
    $data->idForPayment=$id;
    $data->hasUnderpayed=$underpaid;
    $data->comRelease=$commRell;
    $data->save();
  }
  private function updateTotalPaid($cp_id,$newTotal)
  {
    $data=Client_Property::find($cp_id);
    $data->totalPaid=$newTotal;
    $data->save();
  }
  private function updatePaymentUnderPaid($idForPayment)
  {
    $data = DB::table('commissions')
              ->where('id', $idForPayment)
              ->update(['underpaid' => '0']);
  
  }
  // private function updatePaymentUnderPaid2($idForPayment,$monthly)
  // {
  //   $data=Payment::find($idForPayment);
  //   $data->payment=$monthly;
  //   $data->save();
  // }

  private function nowFullPaid($cp_id)
  {
    $data=Client_Property::find($cp_id);
    $data->isFullyPaid=1;
    $data->save();
  }

  public function releaseComm($id,$cp_id,$ornum)
  {
    $user=  Auth::user()->name;
    $data=Commission::find($id);
    $ldate = date('Y-m-d');
    $data->releaseDate=$ldate;
    $data->isRelease='1';
    // $data->systemPay2=50;
    $data->save();
    // $this->storeBillingCom($id,$cp_id,$ornum);
    // Redirect::away("/fpdf/comm.php/?save=$id&user=$user");
    header("Location: /fpdf/comm.php/?save=$id&user=$user");
die();
  }
  private function storeBillingCom($id,$cp_id,$ornum)
  {
    $type="commission";
    $bill = new Bill();

     $bill->id =  $id;
     $bill->cp_id = $cp_id;
     $bill->type = $type;
     $bill->orcn =$ornum;
  $bill->save();
  }
  public function releaseCommDate(Request $request)
  {
    $user=  Auth::user()->name;
$id = $request->agent_id;
$date = $request->date;
    header("Location: /fpdf/commDate.php/?save=$id&user=$user&date=$date");
die();
  }
  public function getUnclaimed(Request $request)
  {
$date = date('Y-m-d');
    $agent_id = $request->agent_id;
      $agentz = Agent::find($agent_id);
    $data = DB::table('commissions')
    ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
    ->join('payments','payments.id', '=', 'commissions.id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'client__properties.isSign','commissions.amount','client__properties.PlanTerms','commissions.cmid','commissions.cp_id','commissions.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.underpaid','commissions.created_at','agents.agent_id')
    ->where('commissions.agent_id', '=', $agent_id)
    // ->orWhere('client__properties.shID', '=', $agent_id)
    // ->orWhere('client__properties.sdID', '=', $agent_id)
    ->where('commissions.amount', '>', 0)
    ->where('commissions.isRelease', '=', '0')
    ->where('commissions.isDelete', '=', '0')
    ->get();
    // dd($data);
      return view('Commission.agentcommission', compact('data', 'agentz'))->with('count',1)->with('initial',0)->with('initial1',0)->with('initial3',0)->with('finalCom',0);
  }
  public function getUnclaimedBank(Request $request)
  {
$date = date('Y-m-d');
    $agent_id = $request->agent_id;
      $agentz = Agent::find($agent_id);
    $data = DB::table('commissions')
    ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
    ->join('payments','payments.id', '=', 'commissions.id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'client__properties.isSign','commissions.amount','client__properties.PlanTerms','commissions.cmid','commissions.cp_id','commissions.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'commissions.created_at','agents.agent_id')
    ->where('commissions.agent_id', '=', $agent_id)
    ->where('commissions.created_at', '!=', $date)

    // ->orWhere('client__properties.shID', '=', $agent_id)
    // ->orWhere('client__properties.sdID', '=', $agent_id)
    ->where('commissions.amount', '>', 0)
    ->where('commissions.isRelease', '=', '0')
    ->where('commissions.isDelete', '=', '0')
    ->get();
    // dd($data);
      return view('Commission.agentbankcommission', compact('data', 'agentz'))->with('count',1)->with('initial',0)->with('initial1',0)->with('initial3',0)->with('finalCom',0);
  }

  public function getRelease(Request $request)
  {
$selectvalue="";
    $agent_id = $request->agent_id;
    $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);
      $balance ="0";
      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','agents.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      // ->orWhere('client__properties.shID', '=', $agent_id)
      // ->orWhere('client__properties.sdID', '=', $agent_id)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->get();



    // dd($data);
      return view('Commission.commissionHistory', compact('data', 'agentz','selectvalue','balance'))->with('balance','')->with('count',1)->with('gselect',"")->with('agent_id',$agent_id)->with('date',$date)->with('initial',0)->with('initialAmount',0);
  }
  public function getReleaseClient(Request $request)
  {

    $agent_id = $request->agent_id;
    $gg= $request->client;
  $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);

      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','client__properties.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      ->where('client__properties.cp_id', '=', $gg)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->get();
      $balance = DB::table('client__properties')
        ->select('client__properties.commTotal')
        ->where('client__properties.cp_id' , '=', $gg)
        ->get();
        // dd($balance);
        $select = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS name")),'client__properties.cp_id')
        ->where('client__properties.agent_id', '=', $agent_id)
        ->get();
        $selectvalue = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS namez")))
        ->where('client__properties.agent_id', '=', $agent_id)
        ->where('client__properties.cp_id', '=', $gg)
        ->get();

    // dd($data);
      return view('Commission.commissionHistory', compact('data', 'agentz','select','selectvalue',))->with('date',$date)->with('balance',$balance[0]->commTotal)->with('gselect',$gg)->with('agent_id',$agent_id)->with('count',1)->with('initial',0);
  }

  public function getReleaseDate(Request $request)
  {

    $agent_id = $request->agent_id;
    $date = $request->relDate;
      $agentz = Agent::find($agent_id);

      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','commissions.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      ->where('commissions.releaseDate', '=', $date)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->get();







    // dd($data);
      return view('Commission.commissionHistory', compact('data', 'agentz'))->with('balance','')->with('date',$date)->with('agent_id',$agent_id)->with('count',1)->with('initial',0);
  }

  public function listPaid(Request $request)
  {
    $name =  Auth::user()->name;
    $cp_id = $request->cp_id;
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')

    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('payments.otherpayment','payments.payment','payments.paymentName', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at')
    ->where('client__properties.cp_id', '=', $cp_id)
        ->where('payments.isActive', '=', 1)
    ->get();
// dd($data);
    $date = DB::table('client__properties')
    ->where('client__properties.cp_id', '=', $cp_id)

    ->get();
    $finaldate = ($date[0]->created_at);
    $paymentsDetails = DB::table('client__properties')
    ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('clients','clients.client_id', '=', 'client__properties.client_id')
    ->join('properties','properties.propId', '=', 'propertylists.propId')
    ->where('client__properties.cp_id', '=', $cp_id)
    ->where('client__properties.isActive', '=', 1)
    ->select('properties.propertyName','client__properties.cp_id','client__properties.comRelease','client__properties.created_at','propertylists.lot','propertylists.block','clients.firstName', 'clients.firstName', 'clients.lastName')
    ->get();
    // dd($data);
      $date = date("m-d-y");
      return view('clientProperties.clientPaymentHistory', compact('data','paymentsDetails'))->with('count',1)->with('initial',0)->with('date',$finaldate)->with('repeat',$date)->with('name',$name);
  }
}
