<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\User;
use App\Models\share;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use DataTables;

class PropertyController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function proplist(Request $request)
  {
    //  $id = Auth::id();
    // $email = DB::table('users')
    // ->select('email')
    // ->where('id',$id)
    // ->get();
    // dd($email);
   $data = DB::table('properties')->paginate(100);
    return view('property.propertylist',['data'=>$data])->with('count',1);
  }
  public function EditProp(Request $request)
  {

    $id = $request->input('id');
    $name = $request->input('propNamez');
    $add = $request->input('addressz');


      $data1=Property::find($id);
      $data1->propertyName=$name;
      $data1->address=$add;
      $data1->save();


    return redirect()->back()->with('message', 'Property Edited Successfully');
  }
  public function store(Request $request)
  {

  $request->validate([
    'propertyName' => 'required',
    'address' => 'required',
  ]);
  $role =  Auth::user()->role;
  if($role!="Admin")
{
return redirect()->back()->withErrors(['msg' => 'NEED ADMIN RIGHTS!!']);
}
if($request->input('checkPercentage')!=2)
{
  return redirect()->back()->withErrors(['msg' => 'TOTAL COMMISSION PERCENTAGE MUST BE EQUALS TO 2!']);
}
  $property = new Property();
  $property->propertyName = $request->input('propertyName');
  $property->address = $request->input('address');
  $property->gc1 = $request->input('agent1');
  $property->gc2 = $request->input('agent2');
  $property->gc1Per = $request->input('aper1');
  $property->gc2Per = $request->input('aper2');
  $property->save();
  return redirect()->back()->with('message', 'Parent Property Added Successfully');


    // return view('property.propertylist');
  }
  //partner imap_savebody
  public function storePartner($partnerid,$property,$percentage)
  {
  $partners = new share();
  $partners->percentage = $percentage;
  $partners->propID =$property;
  $partners->partnerId =$partnerid;
  $partners->save();

    // return view('property.propertylist');
  }
}
