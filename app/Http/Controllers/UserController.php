<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Client_Property;
use App\Models\Propertylist;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

  public function adduser(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
    $user = new User();
  $user->name = $request->input('name');
  $user->email = $request->input('email');
  $user->password = bcrypt($request->input('password'));
  $user->branch = $request->input('branch');
  $user->role = $request->input('role');

  // assign other properties
  $user->save();
   return redirect()->back()->with('message', 'User Added Successfully');
    // return view('dashboard');
  }
  public function loaduserform()
  {
     return view('user.userform');
    // return view('dashboard');
  }
  public function checkpassword(Request $request)
  {
    $cp_id = $request->input('cpFormid');
    $pw = $request->input('password');
    $propID = $request->input('propp');
    $password = DB::table('users')
    ->select('password')
    ->where('role','SuperAdmin')
    ->get();
    $Finalpassword = ($password[0]->password);
    if (Hash::check($pw,$Finalpassword)) {
    $this->forfeit($cp_id,$propID);
    return redirect()->back()->with('message', 'Property Forfeited Successfully');
    }
    else{
      return redirect()->back()->withErrors('Invalid Password');
    }

  }
  Public function forfeit($cp_id,$propertylistid)
  {

      $data=Client_Property::find($cp_id);
      $data->isActive=0;
      $data->save();
      $data2=Propertylist::find($propertylistid);
      $data2->client_id=NULL;
      $data2->status=1;
      $data2->save();

  }
    public function userlists()
    {
      $data = DB::table('users')
      ->where('isActive', '1')
      ->get();
       return view('user.userlisting',['data'=>$data])->with('count',1);
      // return view('dashboard');
    }
    public function profile()
    {

       return view('user.userprofile');
      // return view('dashboard');
    }
    public function uploadAvatar(Request $request)
    {
      if ($request->hasfile('image')) {
        $filename = $request->image->getClientOriginalName();
        $request->image->storeAs('images', $filename, 'public');
        auth()->user()->update(['avatar'=>$filename]);

      }
      return redirect()->back()->with('message', 'User Avatar Updated Successfully');


    }
    public function editUser(Request $request)
    {
        $branch =  $request->input('branch');
        $name =  $request->input('name');
        auth()->user()->update(['name'=>$name,'branch'=>$branch ]);


      return redirect()->back()->with('message', 'User Updated Successfully');


    }

}
