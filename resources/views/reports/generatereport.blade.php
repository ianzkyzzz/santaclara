@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Report Generator </h1>

  @if(session()->has('message'))
  <div class="alert alert-success">{{session()->get('message')}} </div>
  @endif
  <x-alert />
  <form method="Post" action="/dailyBankReport">
     @csrf
  <div class="modal fade" id="daterange" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <!-- start modal#addBlock -->
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"  id="exampleModalLabel">Select Dates</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <label for="product_name"> From</label>
          <input type="date" class="form-control" id="from" name="from" >
          <small id="asNameError" class="form-text text-muted"></small>
          <label for="product_name"> To</label>
          <input type="date" class="form-control" id="to" name="to" >
          <small id="asNameError" class="form-text text-muted"></small>




        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Generate</button>


        </div>
      </div>
    </div>
  </div>

  <select id="reportSelect" class="form-control" name="reportSelect">
<option value="Cash">Cash</option>
<option value="Bank">Bank</option>
<option value="Cheque">Cheque</option>
<option value="Commission">Commission</option>
<option value="releasedCommission">Released Commission</option>
  </select>
  <input type="hidden" id="date" name="date" value="<?php echo date('Y-m-d');?>">
  <a class="btn btn-secondary"  href="#" role="button" data-toggle="modal" data-target="#daterange">Date Range</a>
  <!-- <a class="btn btn-primary" id="report" name="report" href="#" role="button" >Generate</a> -->
    <button type="submit" class="btn btn-primary">Generate</button>
</div>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

  $('#report').on('click', function () {
     // alert("hh");
      var date = $('#date').val();
      var select = $('#reportSelect').val();
      if(select=='Bank'){

      }

  // window.location.href = DOMAIN+"/fpdf/invoice_bill.php?invoice_no="+data+"&"+invoice;
  });
$('#to').on('change', function () {
   // alert("hh");
  var from = $('#from').val();
    var to = $('#to').val();
    var select = $('#reportSelect').val();
       // alert(select);
   // window.location.href ="/fpdf/reports.php?from="+from+"&to="+to;
// window.location.href = DOMAIN+"/fpdf/invoice_bill.php?invoice_no="+data+"&"+invoice;
});

                    $('#Client').on('change', function () {
                                   let client_id = $(this).val();

                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/clientPropertiesList/' + client_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element => {

                                     // console.log(element.getpropertylist_relation.propertyName);
                                       $('#propList').append(`<option value="${element.get_clientproperty_relation.cp_id}">' ${element.getpropertylist_relation.propertyName} Block ${element['block']} Lot ${element['lot']} </option>`);
                                       });
                                   }
                               });
                           });
});
</script>

@endsection
