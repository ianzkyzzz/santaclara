@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              @if($ClientProperty->client4=="" || $ClientProperty->client3=="" || $ClientProperty->client2=="" || $ClientProperty->client4=="")

                <div class="card-header"><h2> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName }}'s Properties</h2>

              @else
                  <div class="card-header"><h3> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName . " ," . $ClientProperty->client4 . " ," . $ClientProperty->client3. " , and " . $ClientProperty->client2 }}'s Properties</h3>
              @endif
                  <h4>{{$ClientProperty->address}}</h4>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Client Property</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">block</th>
      <th scope="col">lot</th>
        <th scope="col">TCP</th>
        <th scope="col">Monthly Amortization</th>
        <th scope="col">Total Paid</th>
        <th scope="col">Balance</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>



@foreach($clientProperties as $items)

<tr>

  <th scope="row" align="center">{{$count++}}</th>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->block}}</td>

  <td align="center">{{$items->lot}}</td>
  <td align="center">{{number_format(round($items->contractPrice,2),2,'.',',')}}</td>
  <td align="center">{{number_format(round($items->monthlyAmortization,2),2,'.',',')}}  </td>
  <td align="center">{{number_format(round($items->totalPaid ,2),2,'.',',') }}</td>
  <td align="center">{{number_format(round((($items->contractPrice - $items->totalPaid)) ,2),2,'.',',') }}</td>


  <td align="center">
    @if($items->isSign == '0')
<a class="btn btn-sm btn-outline-primary" href="{{'/Signed/'.$items->cp_id}}" role="button">Contract Signed</a>
@endif

    <a class="btn btn-sm btn-outline-secondary" id="getRequest" href="#" data-cp_id="{{$items->cp_id}}" data-propid="{{$items->propertylistid}}" data-toggle="modal" data-target="#forfeit" role="button">Forfeit</a>

    <a class="btn btn-sm btn-outline-success" href="{{'/paymenthistory/'.$items->cp_id}}" role="button">Payment History</a>

</td>
</tr>

@endforeach
</tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="forfeit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Super Admin Password</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/tryForfeit">
            @csrf
        <input type="password" class="form-control" id="password" name="password">
        <input type="hidden" class="form-control" id="cpFormid" name="cpFormid">
        <input type="hidden" class="form-control" id="propp" name="propp">

      </div>
      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addProperty" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content ">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Client Property</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/clientProperties/create">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">Select Property</label>
               <select class="form-control" name="property" id="property">
                  <option value="">Select Property</option>
                  @foreach($propertyNameList as $plist)
                 <option value="{{$plist->propId}}">{{$plist->propertyName}}</option>
                @endforeach
               </select>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Block</label>
               <select class="form-control" name="pList" id="pList">
                 <option value="">Select Block</option>

               </select>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Lot</label>
               <select class="form-control" name="lotList" id="lotList">
                 <option value="">Select Lot</option>

               </select>
             </div>
           </div>

           <div class="form-row">

              <div class="form-group col-md-4">
                <label for="product_name">Total Contract Price</label>
                <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                <small id="asNameError" class="form-text text-muted"></small>
                <input type="hidden" id="grant1" name="grant1">
                <input type="hidden" id="grant2" name="grant2">
                <input type="hidden" id="grant1Per" name="grant1Per">
                <input type="hidden" id="grant2Per" name="grant2Per">
                <input type="hidden" id="grant1Com" name="grant1Com">
                <input type="hidden" id="grant2Com" name="grant2Com">
                <input type="hidden" id="percentCounter" name="percentCounter">
                <input type="hidden" id="percentCounter2" name="percentCounter2">
              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">PlanTerms</label>
              <input type="text" class="form-control" id="terms" name="terms" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="product_name">Monthly Amortization</label>
                  <input type="text" class="form-control" id="Monthly" name="Monthly">
                    <input type="hidden" id="cid" name="cid" value="{{$ClientProperty->client_id }}">
                <small id="asNameError" class="form-text text-muted"></small>
              </div>
            </div>
            <div class="form-row">

<div class="form-group col-md-6 mx-auto">
    <label for="Agent_name">Sales Director1</label>
    <select style="width:240px;" id="salesDirector1" name="salesDirector1">
       <option value="">Select Agent</option>

    </select>
  </div>
<div class="form-group col-md-6 mx-auto">
  <label for="product_name">Sales Director2</label>
  <select style="width:240px;" id="salesDirector2" name="salesDirector2">
     <option value="">Select Agent</option>

  </select>
  <small id="asNameError" class="form-text text-muted"></small>
</div>
</div>
<div class="form-row">

<div class="form-group col-md-3 mx-auto">
  <label for="product_name">Percent</label>
    <input type="text" class="form-control" id="sd1" name="sd1">
  <small id="directError" class="form-text text-muted"></small>
</div>
<div class="form-group col-md-3 mx-auto">
  <label for="product_name">Commission</label>
    <input type="text" class="form-control" id="salesDirectorValue1" name="salesDirectorValue1" readonly>
  <small id="asNameError" class="form-text text-muted"></small>
</div>
<div class="form-group col-md-3 mx-auto">
  <label for="product_name">Percent</label>
    <input type="text" class="form-control" id="sd2" name="sd2">
  <small id="asNameError" class="form-text text-muted"></small>
</div>
<div class="form-group col-md-3 mx-auto">
  <label for="product_name">Commission</label>
    <input type="text" class="form-control" id="salesDirectorValue2" name="salesDirectorValue2" readonly>
  <small id="asNameError" class="form-text text-muted"></small>
</div>
</div>
             <div class="form-row">

               <div class="form-group col-md-4 mx-auto">
                   <label for="Agent_name">Direct Agent</label>
                   <select style="width:240px;" id="directAgent" name="directAgent">
                      <option value="">Select Agent</option>

                   </select>
                 </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Unit Manager</label>
                 <select style="width:240px;" id="unitmanager" name="unitmanager">
                    <option value="">Select Agent</option>

                 </select>
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="Unit">Manager</label>
                 <select class="form-control" style="width:240px;" name="manager" id="manager">
                   <option value="">Select Agent</option>

                 </select>
               </div>

               </div>
               <div class="form-row">

                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="dcper" name="dcper" value="0">
                     <small id="directError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="dcom" name="dcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="umper" name="umper"  value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="umcom" name="umcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="mper" name="mper"  value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="mcom" name="mcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Due Date</label>
                       <input type="text" class="form-control" id="due" name="due">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Com Release</label>
                       <input type="text" class="form-control" id="comRel" name="comRel">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                   <label for="product_name">Custom Date Applied</label>
                   <input type="date" class="form-control" id="date" name="date" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                   <label for="product_name">Reservation</label>
                       <input type="text" class="form-control" id="res" name="res" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                   <label for="product_name">OR NUM</label>
                       <input type="text" class="form-control" id="OR" name="OR" value="000-0000-000">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>

                 </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<
<!-- Modal for Assume -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#property').on('change', function () {
                 let prop_id = $(this).val();
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Processing...</option>`);
                 $.ajax({
                 type: 'GET',
                 url: '/propertyLists/' + prop_id,
                 success: function (response) {
                 var response = JSON.parse(response);
                 // console.log(response);
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Select Block*</option>`);
                 response.forEach(element => {
                     $('#pList').append(`<option value="${element['block']}">Block ${element['block']}</option>`);
                     });
                 }
             });
         });
         $('#forfeit').on('show.bs.modal', function (event) {
             var button = $(event.relatedTarget)
             var cp_id = button.data('cp_id')
             var propid = button.data('propid')
             $('#cpFormid').val(cp_id);
             $('#propp').val(propid);

     propp
         });
         $('#assume').on('show.bs.modal', function (event) {
             var button = $(event.relatedTarget)
             var cp_id = button.data('cp_id')
             var propid = button.data('propid')
             var propnym = button.data('propnym')   
             alert(propnym);
                
             $('#cpFormid').val(cp_id);
             $('#propp').val(propid);
             $('#prop').text('Property Name: ' + propnym);
             

     propp
         });
         $('#pList').on('change', function () {
                        let block = $(this).val();
                        var prop_id = $('#property').val();
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Processing...</option>`);
                        $.ajax({
                        type: 'GET',
                        url: '/propertyListsActive/' + block +'/'+ prop_id ,
                        success: function (response) {
                        var response = JSON.parse(response);
                        // console.log(response);
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                        response.forEach(element => {
                            $('#lotList').append(`<option value="${element['propertylistid']}">Lot ${element['lot']}</option>`);
                            });
                        }
                    });
                });

                $('#lotList').on('change', function () {
                               let propertylist = $(this).val();
                               $.ajax({
                               type: 'GET',
                               dataType: 'json',
                               url: '/propertyListsView/' + propertylist ,
                               success: function (data) {
                               // var data = JSON.parse(data);
                               console.log(data[0]);
                                $('#contractPrice').val(data[0].contractPrice);
                                $('#grant1').val(data[0].gc1);
                                $('#grant2').val(data[0].gc2);
                                $('#grant1Per').val(data[0].gc1Per);
                                $('#grant2Per').val(data[0].gc2Per);

                               //alert(data[0].contractPrice);


                               }
                           });
                           $.ajax({
                           type: 'GET',
                           dataType: 'json',
                           url: '/propertyListsView/' + propertylist ,
                           success: function (data) {
                           // var data = JSON.parse(data);
                           console.log(data[0]);
                            $('#contractPrice').val(data[0].contractPrice);
                           //alert(data[0].contractPrice);


                           }
                       });
                       });
                       //function for fetching manager and unit manager
                       $('#directAgent').on('change', function () {
                                      let agentid = $(this).val();
                                  $.ajax({
                                  type: 'GET',
                                  dataType: 'json',
                                  url: '/agentFind/' + agentid ,
                                  success: function (data) {
                                  // var data = JSON.parse(data);
                                  console.log(data[0]);
                                   // $('#manager').val(data[0].manager);
                                   // $('#manager').val('2').trigger('change');
                                   var managerid = data[0].manager;
                                   var unitmanagerid = data[0].unitmanager;


                                   // AJAX 22222
                                   $.ajax({
                                   type: 'GET',
                                   dataType: 'json',
                                   url: '/agentFind/' + managerid ,
                                   success: function (data1) {
                                   // var data = JSON.parse(data);
                                   console.log(data1[0]);
                                    // $('#manager').val(data[0].manager);
                                    // $('#manager').val('2').trigger('change');

                                    var name = data1[0].AgentFname + ' ' + data1[0].AgentLname ;
                                    var $option = $("<option selected></option>").val(managerid).text(name);
                                       $('#manager').append($option).trigger('change')

                                    // $('#manager').select2('1','asp');
                                    // $('#manager').select2('val', data[0].manager);
                                    // $('[name=manager]').val(data[0].manager);
                                    // $("#manager").val(data[0].manager).trigger('change');
                                    // $("#manager").select2('data', { id:data[0].manager, text: "Hello!"});
                                   // alert(data[0].manager);


                                   }
                                 });
                                   //AJAX 22222
                                   // AJAX 33333
                                   $.ajax({
                                   type: 'GET',
                                   dataType: 'json',
                                   url: '/agentFind/' + unitmanagerid ,
                                   success: function (data2) {
                                   // var data = JSON.parse(data);
                                   console.log(data2[0]);
                                    // $('#manager').val(data[0].manager);
                                    // $('#manager').val('2').trigger('change');

                                    var name1 = data2[0].AgentFname + ' ' + data2[0].AgentLname ;
                                    var $option1 = $("<option selected></option>").val(unitmanagerid).text(name1);
                                       $('#unitmanager').append($option1).trigger('change')

                                    // $('#manager').select2('1','asp');
                                    // $('#manager').select2('val', data[0].manager);
                                    // $('[name=manager]').val(data[0].manager);
                                    // $("#manager").val(data[0].manager).trigger('change');
                                    // $("#manager").select2('data', { id:data[0].manager, text: "Hello!"});
                                   // alert(data[0].manager);


                                   }
                                 });
                                   //AJAX 33333
                                   // $('[name=manager]').val(data[0].manager);
                                   // $("#manager").val(data[0].manager).trigger('change');
                                   // $("#manager").select2('data', { id:data[0].manager, text: "Hello!"});
                                  // alert(data[0].manager);


                                  }
                              });
                              });
                      //function for fetching manager and unit manager
                       $('#comRel').on('change', function () {
                                      let release = $(this).val();

                              let price = $('#contractPrice').val();
                              let sd1 = $('#sd1').val();
                              let sd2 = $('#sd2').val();
                              let sdid1 = $('#salesDirector1').val();
                              let sdid2 = $('#salesDirector2').val();
                              if($('#sd1').val()!='' || $('#sd1').val()!='')
                              {
                              gc1 =   (price*($('#sd1').val()/100))/release;
                              gc2 =   (price*($('#sd2').val()/100))/release;
                              $('#grant1').val(sdid1);
                              $('#grant2').val(sdid2);
                           
                              $('#salesDirectorValue1').val(gc1);
                              $('#salesDirectorValue2').val(gc2);
                              
                              }
                              else{ 
                              gc1 =   (price*($('#grant1Per').val()/100))/release;
                              gc2 =   (price*($('#grant2Per').val()/100))/release;
                              }
                              
                              var dcper = parseFloat($('#dcper').val());
                              var umper = parseFloat($('#umper').val());
                              var mper = parseFloat($('#mper').val());
                              var dcom = (price*(dcper/100))/release;
                              var umcom = (price*(umper/100))/release;
                              var mcom = (price*(mper/100))/release;
                          
                              var percentt = dcper + umper + mper;
                            //  alert(dcom);
                             $('#percentCounter').val(percentt);
                             $('#dcom').val(dcom);
                              $('#umcom').val(umcom);
                              $('#mcom').val(mcom);
                              $('#grant1Com').val(gc1);
                              $('#grant2Com').val(gc2);
                             if(percent>8)
                             {
                              $("#directError").html("<span class='text-danger'>Up to 8% Only!</span>");
                              $("#dcper").addClass("border-danger");
                              $("#dcper").addClass("border-danger");
                              $("#mper").addClass("border-danger");
                             }
                             else{
                              $("#directError").html("");
                              $("#dcper").removeClass("border-danger");
                              $("#dcper").removeClass("border-danger");
                              $("#mper").removeClass("border-danger");
                             }
                             
                              });

                       $('#res').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });
                       $('#terms').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });
                      $('#directAgent').select2({
                        ajax:{
                          url:"/agent/getList",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                            _token:CSRF_TOKEN,
                            search: params.term
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#directAgent1').select2({
                        ajax:{
                          url:"/agent/getList",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                            _token:CSRF_TOKEN,
                            search: params.term
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#unitmanager').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#unitmanager1').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                    $('#manager').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#manager1').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#salesDirector2').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#salesDirector1').select2({
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#client').select2({
                      ajax:{
                        url:"/client/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });



});
</script>
@endsection
