@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('ClientList') }}</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Client</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
  <table class="table table-hover" id="clientList">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Address</th>
      <th scope="col">birth Date</th>
      <th scope="col">Mobile</th>
      <th scope="col">Civil Status</th>

      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->firstName . " " . $item->middleName . " " . $item->lastName }}</td>
      <td align="center">{{$item->address}}</td>
      <td align="center">{{date("F j, Y", strtotime($item->birthDate))}}</td>
      <td align="center">{{$item->mobileNumber}}</td>
      <td align="center">{{$item->civilStatus}}</td>

      <td align="center">
        <a class="btn btn-sm btn-outline-success" href="#" data-status="{{$item->civilStatus}}" data-fname="{{$item->firstName}}" data-civilStatus="{{$item->civilStatus}}" data-mobile="{{$item->mobileNumber}}" data-bday="{{$item->birthDate}}" data-address="{{$item->address}}" data-mname="{{$item->middleName}}" data-lname="{{$item->lastName}}" data-client_id="{{$item->client_id}}" data-toggle="modal" data-target="#editClient" role="button">Edit</a>
        <a class="btn btn-sm btn-outline-info" href="{{'/clientProperties/'.$item->client_id.'/list'}}" title="Tooltip on top" role="button">Properties</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title"  id="exampleModalLabel">Add Client</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/client/create">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">First Name</label>
               <input type="text" class="form-control" id="firstName" name="firstName" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Middle Name</label>
             <input type="text" class="form-control" id="middleName" name="middleName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Last Name</label>
             <input type="text" class="form-control" id="lastName" name="lastName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
           </div>
           <div class="form-row">
              <div class="form-group col-md-4">
                <label for="product_name">FB Name</label>
                <input type="text" class="form-control" id="fbName" name="fbName" >
                <small id="asNameError" class="form-text text-muted"></small>

              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">Second Owner(optional)</label>
              <input type="text" class="form-control" id="Client2" name="Client2" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">Third Owner(optional)</label>
              <input type="text" class="form-control" id="Client3" name="Client3" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
            </div>
           <div class="form-row">
           <div class="form-group col-md-8">
               <label for="Prod_categ">Address </label>
            <textarea id="address" name="address" class="form-control"> </textarea>
             </div>
             <div class="form-group col-md-4">
               <label for="product_name"> Birth Date</label>
               <input type="date" class="form-control" id="birthDate" name="birthDate" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             </div>
             <div class="form-row">
             <div class="form-group col-md-8">
                 <label for="Prod_categ">Place of Birth </label>
              <textarea id="placeofBirth" name="placeofBirth" class="form-control"> </textarea>
               </div>
               <div class="form-group col-md-4">
                 <label for="product_name">Gender</label>
                 <select class="form-control" name="gender" id="gender">
                   <option value="">-->Select Gender<--</option>
                   <option value="Male">Male</option>
                   <option value="Female">Female</option>
                   <option value="NotStated">Prefer not to state</option>
                 </select>

                 <small id="asNameError" class="form-text text-muted"></small>

               </div>
               </div>
             <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="product_name"> Mobile Number</label>
                  <input type="text" class="form-control" id="mobileNumber" name="mobileNumber" >
                  <small id="asNameError" class="form-text text-muted"></small>

                </div>
                <div class="form-group col-md-4 mx-auto">
                  <label for="Unit">Email Address</label>
                <input type="text" class="form-control" id="emailAddress" name="emailAddress" >
                  <small id="asTpypeError" class="form-text text-muted"></small>
                </div>
                <div class="form-group col-md-4 mx-auto">
                  <label for="product_name">Civil Status</label>
                  <select class="form-control" name="civilStatus" id="civilStatus">
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Separated">Separated</option>
                  </select>

                  <small id="asNameError" class="form-text text-muted"></small>
                </div>
              </div>



                 <input type="hidden" id="referenceName" name="referenceName" >
                 <input type="hidden" id="referenceAddress" name="referenceAddress" >



               <input type="hidden" id="referenceContact" name="referenceContact" >


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal this is for edit modal -->
<div class="modal fade" id="editClient" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title"  id="exampleModalLabel">Edit Client</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/client/Edit">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">First Name</label>
               <input type="text" class="form-control" id="EditfirstName" name="EditfirstName" >
               <input type="hidden" class="form-control" id="client_id" name="client_id" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Middle Name</label>
             <input type="text" class="form-control" id="EditmiddleName" name="EditmiddleName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Last Name</label>
             <input type="text" class="form-control" id="EditlastName" name="EditlastName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
           </div>
           <div class="form-row">
           <div class="form-group col-md-4">
               <label for="Prod_categ">Address </label>
            <textarea id="Editaddress" name="Editaddress" class="form-control"> </textarea>
             </div>
             <div class="form-group col-md-4">
               <label for="product_name"> Mobile Number</label>
               <input type="text" class="form-control" id="EditmobileNumber" name="EditmobileNumber" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4">
               <label for="product_name">Civil Status</label>
               <select class="form-control" name="EditcivilStatus" id="EditcivilStatus">
                 <option value="Single">Single</option>
                 <option value="Married">Married</option>
                 <option value="Separated">Separated</option>
               </select>

               <small id="asNameError" class="form-text text-muted"></small>
             </div>
           </div>
             </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- modal  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

  $('#editClient').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var fname = button.data('fname')
      var mname = button.data('mname')
      var lname = button.data('lname')
      var id = button.data('client_id')
      var address = button.data('address')
      var mobile = button.data('mobile')
      var status = button.data('status')






      // // alert(title)
      $('#client_id').val(id);
      $('#EditfirstName').val(fname);
      $('#EditmiddleName').val(mname);
      $('#EditlastName').val(lname);
      $('#Editaddress').val(address);
      $('#EditmobileNumber').val(mobile);
      $('#EditcivilStatus').val(status);







      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });

  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>
@endsection
