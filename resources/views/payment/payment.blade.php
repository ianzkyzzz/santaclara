@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Payment') }}</h2></div>
                @if(session()->has('message'))
                <div class="alert alert-success">{{session()->get('message')}} </div>
                @endif
                <x-alert />
                @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif

                <div class="card-body">
                  @if (Route::has('login'))

                    <div class="container-fluid">
                      <div class="container-fluid">
                        <form method="post" action="/payment/store">
                           @csrf

                      <div class="container">

                       <div class="form-row">
                         <div class="card-body" id="map" style="height: 450px;display: none">
                         <img src="{{asset('/img/full.png')}}" alt="{{asset('/storage/images/avatar.png')}}" style="vertical-align:middle"/>
                         </div>
                          <div class="form-group col-md-4">

                            <label for="Agent_name">Client Name</label>
                            <select name="Client" class="custom-select"  id="Client" >
                               <option  value="" >Select Client</option>

                            </select>

                          </div>
                          <div class="form-group col-md-4 mx-auto">
                            <label for="Unit">Property</label>
                            <select class="form-control" name="propList" id="propList">


                            </select>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">Monthly Amortization</label>
                     <input type="text" class="form-control" id="monthlyAmortization" name="monthlyAmortization" readonly>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">PlanTerms</label>
                          <input type="text" class="form-control" id="terms" name="terms" readonly >
                            <small id="asTpypeError" class="form-text text-muted"></small>
                          </div>
                        </div>

                        <div class="form-row">

                           <div class="form-group col-md-3">

                             <label for="product_name">Total Contract Price</label>
                             <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>   <div class="form-group col-md-2">
                                <label for="product_name">Mode of Payment</label>
                                <select name="PaymentMethod" class="form-control"  id="PaymentMethod">

                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                    <option value="Cheque">Cheque</option>
                                </select>
                                <small id="asNameError" class="form-text text-muted"></small>
                              </div>
                              <div class="form-group col-md-2 mx-auto">
                               <label for="product_name">Bank</label>
                               <select name="bank" class="form-control"  id="bank">
                                  <option value="">Select Bank</option>
                                   <option value="PNB">PNB</option>
                                   <option value="METROBANK">METROBANK</option>
                               </select>

                                <small id="asNameError" class="form-text text-muted"></small>

                              </div>
                           <div class="form-group col-md-2 mx-auto">
                            <label for="product_name">Due Date</label>
                               <input type="text" class="form-control" id="due" name="due" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>
                           <div class="form-group col-md-3 mx-auto">
                             <label for="product_name">Penalty</label>
                             <div class="form-check form-check-inline form-control">

                         <input class="form-check-input position-static" title="Check to Include Penalty" type="radio" id="blankCheckbox" value="1" aria-label="...">
                         <input type="text" class="form-control" id="Penalty" name="Penalty" placeholder="Penalty" value="0" readonly>  <br>

                         </div>

                                 </div>
                         </div>
                          <div class="form-row">
                            <input type="text" class="form-control" style="visibility:hidden" id="PenaltyDescription" name="PenaltyDescription" placeholder="DESCRIPTION FOR PENALTY">
                          </div>
                         <div class="form-row">
                           <div class="form-group col-md-2 mx-auto">
                             <label for="product_name">Total Paid</label>
                               <input type="text" class="form-control" id="Paid" name="Paid" value="0.00">
                             <small id="asNameError" class="form-text text-muted"></small>

                                 </div>
                                 <div class="form-group col-md-2 mx-auto">
                                   <label for="product_name">OR NUMBER</label>
                                     <input type="text" class="form-control" id="OR" name="OR" value="0000000">

                                   <small id="asNameError" class="form-text text-muted"></small>

                                       </div>
                            <div class="form-group col-md-2 mx-auto">
                              <label for="product_name">Amount</label>
                                <input type="text" class="form-control" id="Monthly" name="Monthly">
                                   <input type="hidden" id="dcom" name="dcom">
                                   <input type="hidden" id="umcom" name="umcom">
                                   <input type="hidden" id="mcom" name="mcom">
                                  <input type="hidden" id="comRelease" name="comRelease" value="">
                                  <input type="hidden" id="numberRelease" name="numberRelease" value="">
                                  <input type="hidden" id="counter" name="counter">
                                  <input type="hidden" id="gcom1" name="gcom1">
                                  <input type="hidden" id="gcom2" name="gcom2">
                                  <input type="hidden" id="grant1" name="grant1">
                                  <input type="hidden" id="grant2" name="grant2">
                                  <input type="hidden" id="directAgent" name="directAgent">
                                  <input type="hidden" id="unitManager" name="unitManager">
                                  <input type="hidden" id="manager" name="manager">
                                  <input type="hidden" id="hasUnderpayed" name="hasUnderpayed">
                                  <input type="hidden" id="idForPayment" name="idForPayment">
                                  <input type="hidden" id="underpaid" name="underpaid" value="0">
                                  <input type="hidden" id="underpaidBalance" name="underpaidBalance" value="0">
                                  
                              <small id="asNameError" class="form-text text-muted"></small>

                                  </div>
                                  <div class="form-group col-md-2 mx-auto">
                                    <label for="product_name">Total</label>
                              <div class="totaltopay"><p class="form-control" id="totaltopay">0.00</p></div>
                                    <small id="asNameError" class="form-text text-muted"></small>

                                        </div>




                            <div class="form-group col-md-4 mx-auto">
                              <label for="product_name">Details</label>
                                <textarea class="form-control" id="details" name="details"> </textarea>
                                  <input type="hidden" id="cid" name="cid" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                            </div>

                            </div>
                            <div class="form-row">
                            <div class="form-group col-md-4 ">

                  <label for="Agent_name">Multipler(months)</label>
                  <input type="text" class="form-control"  id="multiplier" name="multiplier" value="1" readonly>

                  </div>
                  <div class="form-group col-md-4 ">

<label for="Agent_name">Monthly</label>
<input type="text" class="form-control"  id="FinalMonthly" name="FinalMonthly" readonly>

</div>
<div class="form-group col-md-4 ">

<label for="Agent_name">Balance Last Payment</label>
<input type="text" class="form-control" id="BalanceMonthly" name="BalanceMonthly" readonly>   

</div>

                           
                          </div> 
                          <div class="form-group col-md-3">

  <label for="product_name">Custom Date</label>
  <input type="date" class="form-control" id="date" name="date">
  <small id="asNameError" class="form-text text-muted"></small>
</div>
</div>

                          
                                  <input type="hidden"  id="cusCommission" name="cusCommission">

                                 

                        <button type="submit"  class="btn btn-primary">Save Payment</button>
                      </form>
                      </div>
                      </div>
                      <div class="form-row">


                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

                      $('#Client').select2({
                        ajax:{
                          url:"{{route('client.getClientList')}}",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                           _token:CSRF_TOKEN,
                            search: params.term,
                              col: 'vdn'
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });

$('#propList').on('change', function () {
   let cp_id = $(this).val();
   $.ajax({
   type: 'GET',
   dataType: 'json',
   url: '/clientPropertyFetch/' + cp_id ,
   success: function (data) {
     console.log(data[0]);
         $('#monthlyAmortization').val(data[0].monthlyAmortization);
         $('#contractPrice').val(data[0].contractPrice);
         $('#terms').val(data[0].PlanTerms);
         $('#due').val(data[0].dueDate);
         $('#dcom').val(data[0].directCommission);
         $('#umcom').val(data[0].unitManagerCommission);
         $('#mcom').val(data[0].managerCommission);
         $('#Monthly').val(data[0].monthlyAmortization);
         $('#FinalMonthly').val(data[0].monthlyAmortization - data[0].BalanceMonthly);
         var underPaidBalance =(data[0].monthlyAmortization-(data[0].monthlyAmortization - data[0].BalanceMonthly));
         $("#underpaidBalance").val(underPaidBalance);
         
         $('#Paid').val(data[0].totalPaid);
         $('#comRelease').val(data[0].comRelease);
         $('#directAgent').val(data[0].DC);
         $('#unitManager').val(data[0].UMC);
         $('#manager').val(data[0].MC);
         $('#gcom1').val(data[0].GC1);
         $('#gcom2').val(data[0].GC2);
         $('#grant1').val(data[0].grant1);
         var total = data[0].monthlyAmortization;
         if(underPaidBalance>0){
          $("#underpaid").val('1');
         }
         $('#grant2').val(data[0].grant2);
         $('#numberRelease').val(data[0].comnum);
         $('#counter').val(data[0].counter);
         $('#hasUnderpayed').val(data[0].hasUnderpayed);
         $('#idForPayment').val(data[0].idForPayment);
         $('#BalanceMonthly').val(data[0].BalanceMonthly);
       

         $('#totaltopay').html('<b>'+ (total) +'</b>');
         var x = (data[0].contractPrice)-(data[0].totalPaid);
         $('#Balance').val((data[0].contractPrice)-(data[0].totalPaid));


if (x<=0) {
  $('#Monthly').prop('readonly', true);
  $('#OR').prop('readonly', true);
  $('#details').prop('readonly', true);
  $('#Monthly').val('FULLY PAID');
  $('#OR').val('FULLY PAID');
  $('#details').val('FULLY PAID');
  $("#map").show();
}
else {
  {
    $("#map").hide();
  }
}


   //alert(data[0].contractPrice);


   }
});

});
                    $('#blankCheckbox').on('change', function () {

                      var pen = parseFloat(($('#monthlyAmortization').val()*0.03) );
                      var mon = parseFloat($('#monthlyAmortization').val());
                      if(this.checked) {
                      $('#Penalty').val(($('#monthlyAmortization').val()*0.03).toFixed(2));
                      $("#totaltopay").html("<b>"+ (pen+mon).toFixed(2) +"</b>");
                       PenaltyDescription.style.visibility = 'visible';

                      }
                      else {
                          $('#Penalty').val(0);
                      }
                    });
//

$('#Monthly').on('change', function () {

   let mon = parseFloat($(this).val());
   var monAmort = parseFloat($('#monthlyAmortization').val()); 
   var lastBalance = parseFloat($('#BalanceMonthly').val()); 
  var pen = parseFloat($('#Penalty').val());

  var multiplier = parseInt((mon-lastBalance)/monAmort);
  if(multiplier==0)
  {
    var penFinal = pen;
    var finalMultiplier = multiplier + 1;
    if(mon==monAmort){
      var finalMon = (mon-lastBalance).toFixed(2);
    }
    else{
      var finalMon = (mon-lastBalance).toFixed(2);
    }
    
  }
  else
  {
    var penFinal = pen * multiplier;
    var finalMon = (mon/multiplier).toFixed(2);
    var finalMultiplier = multiplier;
  }
  if(finalMon<monAmort)
  {
    $("#underpaid").val('1');
    var underPaidBalance = monAmort - finalMon;
    $("#underpaidBalance").val(underPaidBalance);
  }
  else
  {
    $("#underpaid").val('0');
    
    $("#underpaidBalance").val('0');
  }
  
  $("#multiplier").val(finalMultiplier);
  $("#FinalMonthly").val(finalMon);
  
  $("#totaltopay").html("<b>"+ (penFinal+mon).toFixed(2) +"</b>");

});
                    $('#Client').on('change', function () {
                                   let client_id = $(this).val();

                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/clientPropertiesList/' + client_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element =>  {

                                     // console.log(element.getpropertylist_relation.propertyName);
                                       $('#propList').append(`<option value="${element['cp_id']}"> ${element['propertyName']} Block ${element['block']} Lot ${element['lot']} </option>`);
                                       });
                                   }
                               });
                           });
});
</script>
@endsection
