@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{$property->propertyName}}</h2>
                  <input type="hidden" name="propId" id="propId" value="{{$property->propId}}">
                  <h5>{{$property->address}}</h5>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                  @if (Auth::user()->role == 'Admin')
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add New Lot</button>
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addBlock">Add Block</button>
@endif
<div class="form-row">
   <div class="form-group col-md-2">
     <label for="product_name">Select Block</label>
     <select class="form-control" name="changeBlock" id="changeBlock">
       @if($block=="")
       <option value="">Select Block</option>
       @else
        <option value="{{$block}}">Block {{$block}}</option>
        @endif
       @foreach($selectData as $select)
       <option value="{{$select->block}}">Block {{$select->block}}</option>
       @endforeach
     </select>

   </div>
 </div>

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table table-striped table-hover" id="propertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Block</th>
      <th scope="col">Lot</th>
      <th scope="col">Contract Price</th>
        <th scope="col">Area Size</th>
        <th scope="col">Price Per SQM</th>
        <th scope="col">Status</th>
        @if (Auth::user()->role == 'Admin')
      <th scope="col">Action</th>
      @endif
    </tr>
  </thead>
  <tbody>

    @foreach($data as $items)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$items->block}}</td>
      <td align="center">{{$items->lot}}</td>
      <td align="center">{{number_format(round($items->contractPrice,2),2,'.',',')}}</td>
      <td align="center">{{number_format(round($items->areasize,2),2,'.',',')}}</td>
      <a href="#" hidden>

      </a>


      <td align="center">  <a class="primary"  href="#" role="button">{{$items->priceSQM}}</a></td>
      @if(($items->status)==1)
      <td align="center">  <a class="primary" href="#" role="button">Available</a></td>
      @else
      <td align="center">  <a class="primary" href="#" role="button">Sold to: {{$items->firstName .' ' . $items->lastName}}</a></td>
      @endif
    
      <td align="center">
      @if (Auth::user()->role == 'Admin')
        <a class="btn btn-sm btn-outline-success" href="#" data-size="{{$items->areasize}}" data-lot="{{$items->lot}}" data-block="{{$items->block}}" data-sqm="{{$items->priceSQM}}" data-propid="{{$items->propertylistid}}" data-Parentpropid="{{$items->propId}}" data-toggle="modal" data-target="#editProperty" role="button">Edit</a>

        <a class="btn btn-sm btn-outline-danger unlist" data-propid2="{{$items->propertylistid}}"  title="Cannot Unlist if Property is already filled" href="#" role="button">Unlist</a>
        @endif
      </td>

    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<div class="modal fade" id="editProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/edit">
            @csrf
        <div class="form-group">


          <label for="exampleInputEmail1">Block</label>
          <input type="text" class="form-control" id="block3" name="block3"  aria-describedby="emailHelp">
          <input type="hidden" name="prrropid" id="prrropid" />
          <input type="hidden" name="parent" id="parent" />


        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Lot</label>
            <input type="textarea" class="form-control" id="lotEdit" name="lotEdit">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Price Per SQM</label>
        <input type="textbox" class="form-control" id="sqmPriceedit" name="sqmPriceedit">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Lot Size</label>
            <input type="textarea" class="form-control" id="lotsize3" name="lotsize3">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Custom Price</label>
        <input type="textbox" class="form-control" id="fixTCPedit" name="fixTCPedit" title="This will be the TCP of this lot">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"  id="exampleModalLabel">Add Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/create">
           @csrf

  <div class="form-group">
    <label for="exampleInputEmail1">Block</label>
    <input type="text" class="form-control" id="block" name="block"  aria-describedby="emailHelp">
    <input type="hidden" name="parentID" id="parentID" value="{{$propID}}"/>

  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Lot</label>
      <input type="textarea" class="form-control" id="lot" name="lot">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Price Per SQM</label>
  <input type="textbox" class="form-control" id="sqmPrice" name="sqmPrice" value="0.00">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Lot Size</label>
      <input type="textarea" class="form-control" id="lotsize" name="lotsize">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Custom Price</label>
  <input type="textbox" class="form-control" id="fixTCP" name="fixTCP">
  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addBlock" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<!-- start modal#addBlock -->
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"  id="exampleModalLabel">Add Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/Blockcreate">
           @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Block</label>
    <input type="text" class="form-control" id="block2" name="block2"  aria-describedby="emailHelp">
    <input type="hidden" name="parentID2" id="parentID2" value="{{$propID}}"/>
    <input type="hidden" name="frontsqm2val" id="frontsqm2val" value="{{$property->frontsqm}}"/>
    <input type="hidden" name="centersqm2val" id="centersqm2val" value="{{$property->centersqm}}"/>
    <input type="hidden" name="innersqm2val" id="innersqm2val" value="{{$property->innersqm}}"/>
    <input type="hidden" name="cornersqm2val" id="cornersqm2val" value="{{$property->cornersqm}}"/>
    <div class="form-group">
      <label for="exampleInputEmail1">Price Per SQM</label>
    <input type="textbox" class="form-control" id="sqmPrice" name="sqmPrice">
    </div>
  <div class="form-group">
    <label for="exampleInputPassword1">NUmber of Lots</label>
      <input type="textarea" class="form-control" id="Numlot" name="Numlot">
  </div>


      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->
<!-- Modal -->




                    @endif






@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });

  $('#editProperty').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var block = button.data('block')
      var lot = button.data('lot')
      var propid = button.data('propid')
      var size = button.data('size')
      var parent = button.data('parentpropid')
      var sqm = button.data('sqm')

      // // alert(title)
      $('#block3').val(block);
      $('#prrropid').val(propid);
      $('#lotEdit').val(lot);
      $('#lotsize3').val(size);
      $('#parent').val(parent);
      $('#sqmPriceedit').val(sqm);

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });
  $("body").delegate('.unlist','click',function(){
           var id = $(this).attr("data-propid2");
           var r = confirm("Are you sure you want to delete property?");
           if (r == true) {
             txt = "You pressed OK!";
           } else {
             txt = "You pressed Cancel!";
           }

       })
  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>
