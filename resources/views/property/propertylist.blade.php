@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('PropertyList') }}</div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
                    @if (Auth::user()->role == 'Admin')
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Property</button>
@endif
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="propertyTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">Address</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->propertyName}}</td>
      <td align="center">{{$item->address}}</td>
      <td align="center">
        <a class="btn btn-sm btn-outline-info" href="{{'/propertyList/'.$item->propId.'/propertylists'}}" role="button">View PropertyList</a>
        @if (Auth::user()->role == 'Admin')
      <a class="btn btn-sm btn-outline-success" href="#" data-name="{{$item->propertyName}}" data-address="{{$item->address}}" data-Parentpropid="{{$item->propId}}" data-toggle="modal" data-target="#editProperty" role="button">Edit</a></td>
      @endif

    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">
{{$data->links()}}
</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty"  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Property Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post"  action="/property/create" id="form">
           @csrf
           <div class="container">

             <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Property Name</label>
                  <input type="text" class="form-control" id="propertyName" name="propertyName" aria-describedby="emailHelp">


                </div>

  <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Property Address</label>
    <input type="text" class="form-control" id="address" name="address" aria-describedby="emailHelp">

  </div>



</div>
    </div>
        <h3>Sales Director Commission</h3>
        <div class="form-row">
           <div class="form-group col-md-4">
          
             <select style="width:300px;" id="agent1" name="agent1">
                <option value="">Select Sales Director 1</option>

             </select>



             <small id="agError" class="form-text text-muted"></small>
             <small id="agmsgSuccess" class="form-text text-muted"></small>

           </div>
           <div class="form-group col-md-2">
            
             <input type="text" class="form-control" id="aper1" name="aper1" aria-describedby="emailHelp" placeholder="Percent">
             <input type="hidden" id="checkPercentage" name="checkPercentage">


           </div>
           <div class="form-group col-md-4">
         
             <select  style="width:300px;" id="agent2" name="agent2">
                <option class="form-control" value="">Select Sales Director 2</option>

             </select>




           </div>
           <div class="form-group col-md-2">
             <input type="text" class="form-control" id="aper2" name="aper2" aria-describedby="emailHelp" placeholder="Percent">


           </div>
            </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editProperty" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyedit">
            @csrf
        <div class="form-group">
          <label for="exampleInputPassword1">Property Name</label>
            <input type="textbox" class="form-control" id="propNamez" name="propNamez">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
        <input type="textarea" class="form-control" id="addressz" name="addressz" >
        <input type="hidden" id="id" name="id" >
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#form').validate({ // initialize the plugin
        rules: {
            pper1: {
                required: true
            },
        }
    });
$('#form').on('change', function () {
var total =  (parseFloat($('#pper1').val()) + parseFloat($('#pper2').val()) );
var totalp =  (parseFloat($('#aper1').val()) + parseFloat($('#aper2').val()));

$('#checkPercentage').val(totalp);
if(totalp > 2)
{
  $("#agError").html("<span class='text-danger'>Up to 2% Only!</span>");
   $("#aper1").addClass("border-danger");
   $("#aper2").addClass("border-danger");
}
else{
  $("#aper1").removeClass("border-danger");
  $("#aper2").removeClass("border-danger");
   $("#agError").html("");
}


        
            });
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });


/////




$('#agent2').select2({
  ajax:{
    url:"/agent/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#agent1').select2({
  ajax:{
    url:"/agent/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
  $('#editProperty').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var name = button.data('name')
      var id = button.data('parentpropid')
      var address = button.data('address')



      // // alert(title)
      $('#propNamez').val(name);
      $('#id').val(id);
      $('#addressz').val(address);

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });

  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>
@endsection
