@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <h2>{{$agent->AgentFname}} {{$agent->AgentLname}}</h2>

                      <h3>{{ __('SUBAGENT LIST') }}</h3>
                </div>





                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add SubAgent</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

                      <table class="table" id="agetTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Sub Agents Name</th>
      <th scope="col">Sub Agents Address</th>
      <th scope="col">Sub Agents Number</th>

    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->SubAgentName}}</td>
      <td align="center">{{$item->SubAgentAddress}}</td>
      <td align="center">{{$item->SubAgentMobile}}</td>

    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Sub Agents</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/subagent/create">
           @csrf
        <div class="container">

          <div class="form-row">

               <label for="product_name">Name</label>
               <input type="text" class="form-control" id="name" name="name" >
                 <input type="hidden" name="agent_id" id="agent_id" value="{{$agent->agent_id}}">
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-row">

                  <label for="product_name">Address</label>
                  <input type="text" class="form-control" id="address" name="address" >
                  <small id="asNameError" class="form-text text-muted"></small>

                </div>
                <div class="form-row">

                     <label for="product_name">Contact Number</label>
                     <input type="text" class="form-control" id="contact" name="contact" >
                     <small id="asNameError" class="form-text text-muted"></small>

                   </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
