@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"> <h2>{{ __('User List') }} </h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Property</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="propertyTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">User's Name</th>
      <th scope="col">Role</th>
        <th scope="col">Branch</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->name}}</td>
      <td align="center">{{$item->role}}</td>
      <td align="center">{{$item->branch}}</td>
      <td align="center">
        <a class="btn btn-sm btn-outline-danger" href="#" role="button">Delete</a>

    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">
{{$data->links()}}
</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/property/create">
           @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">User's Fullname</label>
    <input type="text" class="form-control" id="propertyName" title="New User will be added" name="propertyName" aria-describedby="emailHelp">

  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="pricepersqm" name="pricepersqm">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Confirm Password</label>
      <input type="password" class="form-control" id="pricepersqm" name="pricepersqm">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
      <input type="textarea" class="form-control" id="pricepersqmcorner" name="pricepersqmcorner">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Role</label>
  <select class="form-control" id="role" name="role">
    <option value="Encoder"> Encoder</option>
    <option value="Admin"> Admin</option>
  </select>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Branch</label>
  <select class="form-control" id="branch" name="branch">
    <option value="Panabo">Panabo</option>
    <option value="Sto.Thomas">Sto.Thomas</option>
  </select>
  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
